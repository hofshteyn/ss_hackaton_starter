import React, { useRef, useState, useEffect } from "react"
import {
    Grid,
    makeStyles,
    Button,
    FormControl,
    FormGroup,
    FormControlLabel,
    Checkbox,
    Typography,
    RadioGroup,
    Radio,
    Box,
    TextField,
} from "@material-ui/core"
import { observer } from "mobx-react"
import { DropzoneArea } from "material-ui-dropzone"

import { dashboardPageStore } from "../DashboardPageStore"
import { DonutChart, XyChart, ClusteredChart } from "../../../modules/charts"
import { charts } from "../../../modules/charts/constants"
import { ChartItem } from "../../../modules/charts/models"

const useStyles = makeStyles(() => ({
    root: {
        "width": "100%",
        "padding": 40,
        "& .MuiDropzoneArea-root": {
            minHeight: "calc(100vh - 360px)",
        },
    },
    previewChip: {
        minWidth: 160,
        maxWidth: 210,
    },
}))

export const CreateChart = observer(() => {
    const classes = useStyles()
    const [isButtonShown, setShowButton] = useState(false)
    const [file, setFile] = useState(null)
    const [selectedChart, setSelectedChart] = useState(charts[0])

    const onFileSelected = (files: any) => {
        console.log("file", files)
        if (files.length) {
            setShowButton(true)
            setFile(files[0])
        }
    }

    const sendFile = () => {
        const formData = new FormData()
        formData.append("file", file)
        dashboardPageStore.convertCsvToJson(formData)
    }

    const handleFieldsChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        console.log(event.target.value)
        dashboardPageStore.setSelectedFields(event.target.value)
    }

    const handleChartTypeChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const chart: ChartItem = charts.find((c) => c.type === event.target.value)
        setSelectedChart(chart)
    }

    const setDescription = (event: React.ChangeEvent<HTMLInputElement>) => {
        dashboardPageStore.setChartDescription(event.target.value)
    }

    const saveChart = () => {
        dashboardPageStore.addChartToDashboard(selectedChart.type)
    }

    const getChart = (): JSX.Element | null => {
        if (dashboardPageStore.chartFeilds.length === 2) {
            if (selectedChart.type === "xyChart") {
                return (
                    <XyChart
                        data={dashboardPageStore.constructedChartData}
                        fields={dashboardPageStore.chartFeilds}
                        description={dashboardPageStore.chartDescription}
                    />
                )
            } else if (selectedChart.type === "DonutChart") {
                return (
                    <DonutChart
                        data={dashboardPageStore.constructedChartData}
                        fields={dashboardPageStore.chartFeilds}
                        description={dashboardPageStore.chartDescription}
                    />
                )
            }
        } else if (dashboardPageStore.chartFeilds.length > 2) {
            return (
                <ClusteredChart
                    data={dashboardPageStore.constructedChartData}
                    fields={dashboardPageStore.chartFeilds}
                    description={dashboardPageStore.chartDescription}
                />
            )
        } else {
            return null
        }
    }

    return (
        <Grid container className={classes.root}>
            {!dashboardPageStore.dataForChart?.length ? (
                <>
                    <Grid item xs={12}>
                        <DropzoneArea
                            showPreviews={true}
                            showPreviewsInDropzone={false}
                            useChipsForPreview
                            previewGridProps={{ container: { spacing: 1, direction: "row" } }}
                            previewChipProps={{ classes: { root: classes.previewChip } }}
                            previewText="Selected files"
                            dropzoneText={"Перетащите .csv файл на выделенную область или щелкните левой кнопкой мыши"}
                            onChange={(files) => onFileSelected(files)}
                        />
                    </Grid>
                    {isButtonShown && (
                        <Grid item xs={12}>
                            <Grid container justifyContent="flex-end" style={{ padding: 20 }}>
                                <Button variant="contained" color="primary" onClick={sendFile}>
                                    Загрузить
                                </Button>
                            </Grid>
                        </Grid>
                    )}
                </>
            ) : (
                <>
                    <Grid item xs={5}>
                        <FormControl component="fieldset">
                            <Box role="div" mb={1}>
                                <Typography>Выберите минимум два поля для отрисовки графика:</Typography>
                            </Box>
                            <FormGroup aria-label="fields">
                                {Object.keys(dashboardPageStore.dataForChart[0]).map((item: string, index: number) => {
                                    return (
                                        <FormControlLabel
                                            key={index}
                                            value={item}
                                            control={<Checkbox onChange={handleFieldsChange} />}
                                            label={item}
                                        />
                                    )
                                })}
                            </FormGroup>
                        </FormControl>
                        <Box role="div" mb={1} mt={2}>
                            <Typography>Добавьте описание:</Typography>
                        </Box>
                        <FormControl>
                            <TextField
                                id="outlined-basic"
                                label="Описание графика"
                                variant="outlined"
                                style={{ minWidth: 460 }}
                                onChange={setDescription}
                            />
                        </FormControl>
                        <Box role="div" mb={1} mt={2}>
                            <Typography>Выберите тип графика:</Typography>
                        </Box>
                        <FormControl component="fieldset">
                            <RadioGroup
                                aria-label="chartType"
                                name="chartType"
                                value={selectedChart.type}
                                onChange={handleChartTypeChange}
                            >
                                {charts.map((chart: ChartItem) => (
                                    <FormControlLabel
                                        key={chart.name}
                                        value={chart.type}
                                        control={<Radio />}
                                        label={chart.name}
                                    />
                                ))}
                            </RadioGroup>
                        </FormControl>
                        <Box role="div" mt={2}>
                            <Button variant="contained" color="primary" onClick={saveChart}>
                                Сохранить
                            </Button>
                        </Box>
                    </Grid>
                    <Grid item xs={7}>
                        {getChart()}
                    </Grid>
                </>
            )}
        </Grid>
    )
})
