import React, { useRef, useState, useEffect } from "react"
import { Grid, makeStyles, Box, Typography, Card, CardContent } from "@material-ui/core"
import { observer } from "mobx-react"

import { dashboardPageStore, Chart } from "../DashboardPageStore"
import { XyChart, DonutChart } from "../../../modules/charts"

const useStyles = makeStyles((theme) => ({
    root: {},
    cardContainer: {
        padding: theme.spacing(4),
    },
    card: {
        "cursor": "pointer",
        "borderRadius": 10,
        "transition": "all .2s ease-in-out",
        "boxShadow":
            "0px 2px 1px -1px rgb(0 0 0 / 20%), 0px 1px 1px 0px rgb(0 0 0 / 14%), 0px 1px 3px 0px rgb(0 0 0 / 12%)",
        "&:hover": {
            transform: "scale(1.05)",
        },
    },
}))

const getChart = (chart: Chart): JSX.Element => {
    switch (chart.type) {
        case "xyChart":
            return <XyChart data={chart.chartData} fields={chart.fields} description={chart.description} />
        default:
            return <DonutChart data={chart.chartData} fields={chart.fields} description={chart.description} />
    }
}

export const Dashboard = observer(() => {
    const classes = useStyles()

    useEffect(() => {}, [])

    return (
        <Grid container className={classes.root}>
            <Grid item xs={12}>
                {!dashboardPageStore.dashboardCharts.length ? (
                    <Box ml={4} mt={10}>
                        <Typography variant="h3">У вас пока нет ни одного графика.</Typography>
                    </Box>
                ) : (
                    <Grid container justifyContent="center">
                        {dashboardPageStore.dashboardCharts
                            .filter((chart) => chart.isShown)
                            .map((chart) => {
                                return (
                                    <Grid item xs={6} className={classes.cardContainer}>
                                        <Card className={classes.card}>
                                            <CardContent>{getChart(chart)}</CardContent>
                                        </Card>
                                    </Grid>
                                )
                            })}
                    </Grid>
                )}
            </Grid>
        </Grid>
    )
})
