import React, { useRef, useState, useEffect } from "react"
import { Box, Checkbox, Grid, makeStyles, Typography } from "@material-ui/core"
import { observer } from "mobx-react"
import Table from "@material-ui/core/Table"
import TableBody from "@material-ui/core/TableBody"
import TableCell from "@material-ui/core/TableCell"
import TableContainer from "@material-ui/core/TableContainer"
import TableHead from "@material-ui/core/TableHead"
import TableRow from "@material-ui/core/TableRow"
import { toJS } from "mobx"

import { dashboardPageStore } from "../DashboardPageStore"
import { charts } from "../../../modules/charts/constants"

const useStyles = makeStyles(() => ({
    root: {},
    table: {},
}))

const columns = [
    { field: "type", headerName: "Тип чарта", width: 150 },
    {
        field: "description",
        headerName: "Описание",
        width: 400,
    },
    {
        field: "isShown",
        headerName: "Показывать на дашборде",
        width: 80,
        editable: true,
    },
]

const getType = (type: string): string => {
    return charts.find((c) => c.type).name
}

export const ChartList = observer(() => {
    const classes = useStyles()

    useEffect(() => {
        console.log("dashboardPageStore.dashboardCharts", toJS(dashboardPageStore.dashboardCharts))
    }, [])

    const handleChange = (ev: React.ChangeEvent<HTMLInputElement>, chartId: number) => {
        dashboardPageStore.setChartShowValue(chartId)
    }

    return (
        <Grid container className={classes.root}>
            <Grid item xs={12}>
                <Box p={2}>
                    <Typography variant="h5">Список чартов:</Typography>
                </Box>
                {dashboardPageStore.dashboardCharts ? (
                    <div style={{ height: 400, width: "100%" }}>
                        <TableContainer>
                            <Table className={classes.table} size="small" aria-label="a dense table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell size="medium">Тип графика</TableCell>
                                        <TableCell size="medium" align="right">
                                            Описание
                                        </TableCell>
                                        <TableCell size="medium" align="right">
                                            Добавлено на дашбоард
                                        </TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {dashboardPageStore.dashboardCharts.map((row) => (
                                        <TableRow key={row.id}>
                                            <TableCell size="medium" component="th" scope="row">
                                                {getType(row.type)}
                                            </TableCell>
                                            <TableCell size="medium" align="right">
                                                {row.description}
                                            </TableCell>
                                            <TableCell
                                                size="medium"
                                                align="right"
                                                children={
                                                    <Checkbox
                                                        checked={row.isShown}
                                                        onChange={(ev) => handleChange(ev, row.id)}
                                                        inputProps={{ "aria-label": "primary checkbox" }}
                                                    />
                                                }
                                            ></TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </div>
                ) : null}
            </Grid>
        </Grid>
    )
})
