import { makeObservable, computed, action, observable, runInAction, toJS, reaction } from "mobx"

import API from "../../api"

export interface Chart {
    id: number
    chartData: any[]
    description: string
    fields: string[]
    isShown: boolean
    type: string
}

export class DashboardPageStore {
    dataForChart: any[] = []

    constructedChartData: any[] = []
    chartFeilds: string[] = []
    chartDescription: string = ""

    chartId = 0
    dashboardCharts: Chart[] = []

    constructor() {
        makeObservable(this, {
            dataForChart: observable,
            constructedChartData: observable,
            chartFeilds: observable,
            chartDescription: observable,
            dashboardCharts: observable,

            setSelectedFields: action,
            setChartDescription: action,
            setChartShowValue: action,
            dropValues: action,
        })

        reaction(
            () => this.chartFeilds,
            (fields) => {
                this.constructedChartData = []
                if (fields.length) {
                    this.dataForChart.forEach((data) => {
                        const objectToPush: any = {}
                        Object.entries(data).forEach((item) => {
                            const [key, value] = item
                            if (fields.includes(key)) {
                                objectToPush[key] = value
                            }
                        })
                        this.constructedChartData.push(objectToPush)
                    })
                    console.log("this.constructedChartData", toJS(this.constructedChartData))
                }
            }
        )
    }

    convertCsvToJson = async (file: FormData) => {
        const data = await API.post("/files/convert", file)
        runInAction(() => {
            console.log("daat", data)
            this.dataForChart = data
        })
    }

    setSelectedFields(value: string) {
        if (this.chartFeilds.includes(value)) {
            this.chartFeilds = this.chartFeilds.filter((i) => i !== value)
        } else {
            this.chartFeilds = [...this.chartFeilds, value]
        }
    }

    setChartDescription(value: string) {
        this.chartDescription = value
    }

    addChartToDashboard(chartType: string) {
        const chart: Chart = {
            id: this.chartId,
            description: this.chartDescription,
            chartData: this.constructedChartData,
            fields: this.chartFeilds,
            isShown: false,
            type: chartType,
        }
        this.dashboardCharts.push(chart)
        this.chartId++
        console.log("this.dashboardCharts", toJS(this.dashboardCharts))
        this.dropValues()
    }

    dropValues() {
        this.setChartDescription("")
        runInAction(() => {
            this.dataForChart = null
        })
        runInAction(() => {
            this.chartFeilds = []
        })
    }

    setChartShowValue(chartId: number) {
        const charts = this.dashboardCharts.map((c) => {
            if (chartId === c.id) {
                c.isShown = !c.isShown
            }
            return c
        })
        this.dashboardCharts = charts
    }
}

export const dashboardPageStore = new DashboardPageStore()
