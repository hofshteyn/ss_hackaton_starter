import React, { useRef, useState, useEffect } from "react"
import { Grid, makeStyles, Tabs, Tab, Typography, Box } from "@material-ui/core"
import { observer } from "mobx-react"

import { Dashboard } from "./Dashboard"
import { ChartList } from "./ChartList"
import { CreateChart } from "./CreateChart"

const useStyles = makeStyles((theme) => ({
    root: {
        height: "calc(100vh - 67px)",
        marginTop: 67,
    },
    tabPanel: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
        display: "flex",
        height: "100%",
        width: "100%",
    },
    tabs: {
        width: 200,
        borderRight: `1px solid ${theme.palette.divider}`,
    },
}))

const TabPanel = (props: any) => {
    const { children, value, index, ...other } = props
    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`vertical-tabpanel-${index}`}
            aria-labelledby={`vertical-tab-${index}`}
            style={{ width: "100%" }}
        >
            {value === index && <div>{children}</div>}
        </div>
    )
}

const a11yProps = (index: number) => {
    return {
        "id": `vertical-tab-${index}`,
        "aria-controls": `vertical-tabpanel-${index}`,
    }
}

const DashboardPage = observer(() => {
    const classes = useStyles()
    const [value, setValue] = React.useState<number>(3)

    const handleChange = (event: any, newValue: number) => {
        setValue(newValue)
    }

    return (
        <Grid container className={classes.root}>
            <Grid item xs={12} className={classes.tabPanel}>
                <Tabs
                    orientation="vertical"
                    variant="scrollable"
                    value={value}
                    onChange={handleChange}
                    aria-label="Vertical tabs example"
                    className={classes.tabs}
                >
                    <Tab label="" disabled />
                    <Tab label="Dashboard" />
                    <Tab label="Список чартов" />
                    <Tab label="Создать чарт" />
                </Tabs>
                <TabPanel value={value} index={1} {...a11yProps(1)}>
                    <Dashboard />
                </TabPanel>
                <TabPanel value={value} index={2} {...a11yProps(2)}>
                    <ChartList />
                </TabPanel>
                <TabPanel value={value} index={3} {...a11yProps(3)}>
                    <CreateChart />
                </TabPanel>
            </Grid>
        </Grid>
    )
})

export default DashboardPage
