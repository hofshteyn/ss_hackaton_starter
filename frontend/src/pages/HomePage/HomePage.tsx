import React, { useEffect } from "react"
import { Grid, makeStyles } from "@material-ui/core"
import { observer } from "mobx-react"
import { useHistory } from "react-router-dom"

import { homePageStore } from "./HomePageStore"
import { RegionList, TotalList } from "../../modules/shared/components"
import { Map } from "../../modules/map/components/Map"
import { mapStore } from "../../modules/map/store"
import { statisticsStore } from "../../modules/statistics/store"

import "mapbox-gl/dist/mapbox-gl.css"

const useStyles = makeStyles(() => ({
    root: {
        height: "calc(100vh - 64px)",
    },
    mapContainer: {
        position: "relative",
        height: "100%",
    },
    map: {
        "position": "absolute",
        "zIndex": 1,
        "top": "64px",
        "right": 0,
        "width": "100%",
        "height": "100%",
        ".mapboxgl-canvas-container": {
            ".mapboxgl-canvas:focus": {
                outline: "none",
            },
        },
    },
}))

const HomePage = observer(() => {
    const classes = useStyles()
    const history = useHistory()

    useEffect(() => {
        statisticsStore.loadCommonStatistics()
    }, [])

    useEffect(() => {
        history.push(`/home/${homePageStore.regionName}`)
    }, [homePageStore.regionName])

    const selectRegion = (regionName: string) => {
        homePageStore.setRegionName(regionName)
        mapStore.selectRegion(regionName)
    }

    return (
        <Grid container className={classes.root}>
            <Grid item xs={12}>
                <RegionList onSelectRegion={selectRegion} regions={statisticsStore.statisticByRegion.regions} />
                <TotalList
                    employees={statisticsStore.statisticByRegion.totalEmployees}
                    averageAge={statisticsStore.statisticByRegion.totalAverageAge}
                />
                <Map onSelectRegion={selectRegion} />
            </Grid>
        </Grid>
    )
})

export default HomePage
