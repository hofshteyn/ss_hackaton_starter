import { makeObservable, computed, action, observable, runInAction } from "mobx"

export class HomePageStore {
	regionName = ""
	constructor() {
		makeObservable(this, {
			regionName: observable.ref,

			setRegionName: action,
		})
	}

	setRegionName(name: string): void {
		this.regionName = name
	}
}

export const homePageStore = new HomePageStore()
