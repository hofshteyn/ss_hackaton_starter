import React, { useEffect } from "react"
import { observer } from "mobx-react"
import * as am4core from "@amcharts/amcharts4/core"
import * as am4charts from "@amcharts/amcharts4/charts"
import am4themes_animated from "@amcharts/amcharts4/themes/animated"
import { Filter } from "../../modules/statistics/components"
import { Grid } from "@material-ui/core"

import { deltaTimeReportStore } from "./DeltaTimeReportStore"
import { deps, stats } from "../../../mocks/DeltaTime"
import { Description } from "../../modules/shared/components"

am4core.useTheme(am4themes_animated)

const DeltaTimeReport = observer(() => {
    const xyChartData = deltaTimeReportStore.xyChartData
    const donutChartData = deltaTimeReportStore.donutChartData

    useEffect(() => {
        let xyChart: any, series

        xyChart = am4core.create("xyChart", am4charts.XYChart)
        xyChart.hiddenState.properties.opacity = 0
        xyChart.data = xyChartData.data

        let title = xyChart.titles.create()
        title.text = `Количество заявок\n[font-size:16px]Всего заявок: [bold]${xyChartData.total}[/]`
        title.fontSize = 18
        title.marginBottom = 10

        let categoryAxis = xyChart.xAxes.push(new am4charts.CategoryAxis())
        categoryAxis.dataFields.category = "department"
        categoryAxis.renderer.grid.template.location = 0
        categoryAxis.renderer.minGridDistance = 30
        categoryAxis.title.text = "Отдел"
        categoryAxis.title.fontSize = 16
        categoryAxis.renderer.labels.template.rotation = -90
        categoryAxis.renderer.labels.template.dy = -10
        categoryAxis.renderer.labels.template.dx = -15

        let valueAxis = xyChart.yAxes.push(new am4charts.ValueAxis())

        series = xyChart.series.push(new am4charts.ColumnSeries())
        series.dataFields.valueY = "values"
        series.dataFields.categoryX = "department"
        series.name = "Количество заявок"
        series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]"
        series.columns.template.fillOpacity = 0.8
        series.columns.template.adapter.add("fill", function (fill: any, target: any) {
            return xyChart.colors.getIndex(target.dataItem.index)
        })

        // Add label
        var labelBullet = series.bullets.push(new am4charts.LabelBullet())
        labelBullet.label.text = "{valueY}"
        labelBullet.locationY = 0.1
        labelBullet.label.hideOversized = true

        let columnTemplate = series.columns.template
        columnTemplate.strokeWidth = 2
        columnTemplate.strokeOpacity = 1
    }, [xyChartData])

    useEffect(() => {
        let donutChart: any

        donutChart = am4core.create("donutChart", am4charts.PieChart)
        donutChart.innerRadius = am4core.percent(40)
        donutChart.radius = am4core.percent(60)
        donutChart.data = donutChartData

        let title = donutChart.titles.create()
        title.text = "Среднее время обработки (дни)"
        title.fontSize = 18

        // Add and configure Series
        let pieSeries = donutChart.series.push(new am4charts.PieSeries())
        pieSeries.dataFields.value = "value"
        pieSeries.dataFields.category = "department"
        pieSeries.slices.template.stroke = am4core.color("#fff")
        pieSeries.slices.template.strokeWidth = 2
        pieSeries.slices.template.strokeOpacity = 1
        pieSeries.slices.template.tooltipText = "{category}: {value.value.formatNumber('#.00')}"
        pieSeries.labels.template.text = "{category} - {value.value.formatNumber('#.00')}"

        // This creates initial animation
        pieSeries.hiddenState.properties.opacity = 1
        pieSeries.hiddenState.properties.endAngle = -90
        pieSeries.hiddenState.properties.startAngle = -90
    }, [donutChartData])

    return (
        <>
            <Filter departments={deps} statuses={stats} />
            <Description
                header={"Сводка по отделам"}
                description={
                    "Среднее время обработки запроса из Jira по отделам, в днях, для отслеживания просадок по времени нахождения в том или ином статусе в определенный момент времени"
                }
            />
            <div className="dashboard">
                <Grid container>
                    <Grid item xs={6}>
                        <div id="xyChart" style={{ height: "500px" }} />
                    </Grid>
                    <Grid item xs={6}>
                        <div id="donutChart" style={{ height: "500px" }} />
                    </Grid>
                </Grid>
            </div>
        </>
    )
})

export default DeltaTimeReport
