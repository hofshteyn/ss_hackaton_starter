import { filterStore } from "../../modules/statistics/components/Filter/FilterStore"
import { Dictionary } from "./../../modules/shared/models"
import { makeObservable, computed, observable, reaction } from "mobx"
import { dataMocks as DeltaTimeMock } from "../../../mocks/DeltaTime"

// ms to days: 1000*60*60*24

interface deltaTimeProps {
    department: string
    number: string
    login: string
    author: string
    date: number
    status: string
    time: number
}

export class DeltaTimeReportStore {
    reportData: Array<deltaTimeProps> = DeltaTimeMock
    filteredData: Array<deltaTimeProps> = DeltaTimeMock
    error = ""

    constructor() {
        makeObservable(this, {
            filteredData: observable.ref,
            error: observable.ref,
            xyChartData: computed,
            donutChartData: computed,
        })

        reaction(
            () => filterStore.getFilters,
            (filters: any) => {
                this.filteredData = this.reportData.filter(
                    ({ status, department }): any =>
                        filters.statuses?.includes(status) && filters.departments?.includes(department)
                )
            }
        )
    }

    get xyChartData(): Dictionary {
        let chartData: Dictionary = {},
            total = 0
        this.filteredData.forEach(({ department }: any) => {
            if (!chartData[department]) {
                chartData[department] = {
                    department,
                    values: 0,
                }
            }
            chartData[department].values++
            total++
        })
        return { data: Object.values(chartData), total }
    }

    get donutChartData(): Dictionary {
        let chartData: Dictionary = {}
        this.filteredData.forEach(({ department, time }: any) => {
            if (!chartData[department]) {
                chartData[department] = {
                    department,
                    items: 0,
                    values: 0,
                }
            }
            chartData[department].items++
            chartData[department].values += time
        })

        return Object.values(chartData).map(({ department, items, values }) => {
            return {
                department,
                value: values / items / (1000 * 60 * 60 * 24),
            }
        })
    }
}

export const deltaTimeReportStore = new DeltaTimeReportStore()
