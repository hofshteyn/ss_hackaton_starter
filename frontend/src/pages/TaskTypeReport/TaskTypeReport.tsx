import React, { useEffect } from "react"
import { observer } from "mobx-react"
import * as am4core from "@amcharts/amcharts4/core"
import * as am4charts from "@amcharts/amcharts4/charts"
import am4themes_animated from "@amcharts/amcharts4/themes/animated"
import { Filter } from "../../modules/statistics/components"
import { Grid } from "@material-ui/core"

import { status } from "../../../mocks/taskType"

import { taskTypeReportStore } from "./TaskTypeReportStore"
import { filterStore } from "../../modules/statistics/components/Filter/FilterStore"
import { Description } from "../../modules/shared/components"

am4core.useTheme(am4themes_animated)

const TaskTypeReport = observer(() => {
    const xyChartData = taskTypeReportStore.xyChartData
    const filterData = taskTypeReportStore.filterData
    const filters = filterStore.getFilters

    useEffect(() => {
        taskTypeReportStore.loadData()
    }, [])

    const interfaceColors = new am4core.InterfaceColorSet()

    useEffect(() => {
        const chart: any = am4core.create("xyChart", am4charts.XYChart)
        chart.data = xyChartData

        // the following line makes value axes to be arranged vertically.
        chart.bottomAxesContainer.layout = "horizontal"
        chart.bottomAxesContainer.reverseOrder = true

        const categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis())
        categoryAxis.dataFields.category = "name"
        categoryAxis.dataFields.employees = "employees"
        categoryAxis.renderer.grid.template.stroke = interfaceColors.getFor("background")
        categoryAxis.renderer.grid.template.strokeOpacity = 1
        categoryAxis.renderer.grid.template.location = 1
        categoryAxis.renderer.minGridDistance = 20
        categoryAxis.dataItems.template.text = `{name}: {employees}`

        filters.statuses?.forEach((value: string, key: number) => {
            const valueAxis1 = chart.xAxes.push(new am4charts.ValueAxis())
            valueAxis1.tooltip.disabled = true
            valueAxis1.renderer.baseGrid.disabled = true
            if (key !== status.length - 1) {
                valueAxis1.marginRight = 30
            }
            valueAxis1.renderer.gridContainer.background.fill = interfaceColors.getFor("alternativeBackground")
            valueAxis1.renderer.gridContainer.background.fillOpacity = 0.05
            valueAxis1.renderer.grid.template.stroke = interfaceColors.getFor("background")
            valueAxis1.renderer.grid.template.strokeOpacity = 1
            valueAxis1.title.text = value

            const series1 = chart.series.push(new am4charts.LineSeries())
            series1.dataFields.categoryY = "name"
            series1.dataFields.valueX = value
            series1.xAxis = valueAxis1
            series1.name = value
            series1.strokeDasharray = "3,3"
            series1.strokeOpacity = 0.7
            const bullet1 = series1.bullets.push(new am4charts.CircleBullet())
            bullet1.tooltipText = "{valueX.value}"
        })

        chart.cursor = new am4charts.XYCursor()
        chart.cursor.behavior = "none"

        // Enable export
        chart.exporting.menu = new am4core.ExportMenu()
    }, [xyChartData])

    return (
        <>
            <Filter departments={filterData} statuses={status} />
            <Description
                header={"Занятость по отделу"}
                description={
                    "Просмотр количества сотрудников, занятых в коммерческих или внутренних задач, а так же, количество сотрудников в отпуске или на больничном в разрезе одного отдела"
                }
            />
            <div className="dashboard">
                <Grid container>
                    <Grid item xs={12}>
                        <div id="xyChart" style={{ height: "calc(100vh - 150px)" }} />
                    </Grid>
                </Grid>
            </div>
        </>
    )
})

export default TaskTypeReport
