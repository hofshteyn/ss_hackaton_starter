import { filterStore } from "../../modules/statistics/components/Filter/FilterStore"
import { Dictionary } from "./../../modules/shared/models"
import { makeObservable, computed, observable, reaction } from "mobx"
import API from "../../api"

const allTypes: Dictionary = {
    "1": "Коммерческая",
    "2": "Внутренняя",
    "3": "Отпуск",
    "4": "Больничный",
}

// ms to days: 1000*60*60*24
export class TaskTypeReportStore {
    reportData: Dictionary = []
    filteredData: Dictionary = []
    filterData: Array<string> = []
    error = ""

    constructor() {
        makeObservable(this, {
            filteredData: observable.ref,
            filterData: observable.ref,
            error: observable.ref,
            xyChartData: computed,
            // donutChartData: computed,
        })

        reaction(
            () => filterStore.getFilters,
            (filters: any) => {
                this.filteredData = this.reportData
                    .filter(({ name }: Dictionary) => filters.departments?.includes(name))
                    .map(({ name, employees, ...rest }: Dictionary) => {
                        const toReturn: any = {
                            name,
                            employees: 0,
                        }
                        filters.statuses?.forEach((value: string) => {
                            toReturn[value] = rest[value]
                            toReturn.employees += rest[value] || 0
                        })
                        return toReturn
                    })
            }
        )
    }

    loadData = async () => {
        try {
            const statistics = await API.post("/statistics/departments/tasks", {})
            const filters: Array<string> = []
            const preFilter = statistics.map(({ name, employees, types }: Dictionary) => {
                filters.push(name)
                return {
                    name,
                    employees,
                    ...Object.fromEntries(types.map((item: Dictionary) => [allTypes[item.type], item.employees])),
                }
            })
            this.reportData = preFilter
            this.filteredData = preFilter
            this.filterData = filters
        } catch (e) {
            console.log("loadData e", e)
        }
    }

    get xyChartData(): Dictionary {
        return this.filteredData
    }
}

export const taskTypeReportStore = new TaskTypeReportStore()
