import { lazy } from "react"

const HomePage = lazy(() => import("./HomePage/HomePage"))
const Statistics = lazy(() => import("./Statistics/Statistics"))
const DashboardPage = lazy(() => import("./DashboardPage/DashboardPage"))
import DeltaTimeReport from "./DeltaTimeReport/DeltaTimeReport"
import TaskTypeReport from "./TaskTypeReport/TaskTypeReport"
import EmploymentReport from "./EmploymentReport/EmploymentReport"
export { HomePage, Statistics, TaskTypeReport, DeltaTimeReport, DashboardPage, EmploymentReport }
