import React, { useEffect } from "react"
import { Switch, Route, useRouteMatch, Redirect } from "react-router-dom"
import { DeltaTimeReport, TaskTypeReport, EmploymentReport } from "../"
import { CssBaseline } from "@material-ui/core"

import "./Statistics.scss"

import { Sidebar } from "../../modules/statistics/components"

const Statistics = () => {
    const { path } = useRouteMatch()
    return (
        <div className="root">
            <CssBaseline />
            <Sidebar />
            <main className="content">
                <Switch>
                    <Route exact path={path}>
                        <Redirect from="/statistics" to="/statistics/deltaTime" />
                    </Route>
                    <Route path={`${path}/deltaTime`}>
                        <DeltaTimeReport />
                    </Route>
                    <Route path={`${path}/taskType`}>
                        <TaskTypeReport />
                    </Route>
                    <Route path={`${path}/employment`}>
                        <EmploymentReport />
                    </Route>
                </Switch>
            </main>
        </div>
    )
}

export default Statistics
