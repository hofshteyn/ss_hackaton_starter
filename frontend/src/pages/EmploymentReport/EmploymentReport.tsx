import React, { useEffect } from "react"
import { observer } from "mobx-react"
import * as am4core from "@amcharts/amcharts4/core"
import * as am4charts from "@amcharts/amcharts4/charts"
import am4themes_animated from "@amcharts/amcharts4/themes/animated"
import { Filter } from "../../modules/statistics/components"
import {
    Grid,
    List,
    ListItem,
    ListItemText,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    Typography,
} from "@material-ui/core"

import { employmentReportStore } from "./EmploymentReportStore"
import { makeStyles } from "@material-ui/styles"
import { Description } from "../../modules/shared/components"

am4core.useTheme(am4themes_animated)

interface EmployeeProps {
    employee: string
    daysOnProject: number
    completed: boolean
}

interface TasksProps {
    name: string
    employees: Array<EmployeeProps>
}

const getPrulal = (number: number) => {
    switch (number % 10) {
        case 2:
        case 3:
        case 4:
            return "человека"
        default:
            return "человек"
    }
}

const useStyles = makeStyles({
    tableHeader: {
        textAlign: "center",
        fontSize: "24px",
        margin: "16px 0",
    },
    headerCell: {
        fontWeight: "bold",
    },
})

const EmploymentReport = observer(() => {
    const classes = useStyles()
    const chartData = employmentReportStore.chartData
    const filterData = employmentReportStore.filterData
    const totalTasks = employmentReportStore.totalTasks
    const tableData = employmentReportStore.tableData

    useEffect(() => {
        employmentReportStore.loadData()
    }, [])

    useEffect(() => {
        let chart: any = am4core.create("xyChart", am4charts.XYChart)
        chart.padding(10, 40, 0, 40)

        // title
        let title = chart.titles.create()
        title.text = `Количество тасков на отдел`
        title.fontSize = 18
        title.marginBottom = 10

        let categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis())
        categoryAxis.renderer.grid.template.location = 0
        categoryAxis.dataFields.category = "name"
        categoryAxis.dataFields.percent = "percent"
        categoryAxis.dataItems.template.text = `{name}`
        categoryAxis.renderer.minGridDistance = 1
        categoryAxis.renderer.inversed = true
        categoryAxis.renderer.grid.template.disabled = true

        let valueAxis = chart.xAxes.push(new am4charts.ValueAxis())
        valueAxis.min = 0

        let series = chart.series.push(new am4charts.ColumnSeries())
        series.dataFields.categoryY = "name"
        series.dataFields.valueX = "tasks"

        series.tooltipText = "{valueX.value}"
        series.columns.template.strokeOpacity = 0
        series.columns.template.column.cornerRadiusBottomRight = 5
        series.columns.template.column.cornerRadiusTopRight = 5

        let labelBullet = series.bullets.push(new am4charts.LabelBullet())
        labelBullet.label.horizontalCenter = "left"
        labelBullet.label.dx = 10
        labelBullet.label.text = "{values.valueX.workingValue}"
        labelBullet.locationX = 1

        series.columns.template.adapter.add("fill", function (fill: any, target: any) {
            return chart.colors.getIndex(target.dataItem.index)
        })

        categoryAxis.sortBySeries = series
        chart.data = chartData.data
    }, [chartData])

    return (
        <>
            <Filter departments={filterData} statuses={[]} />
            <Description
                header={"Коммерческая занятость"}
                description={
                    "Просмотр данных по компании. Количество задач по заказчику в разрезе отдела. Среднее время работы сотрудника на проекте."
                }
            />
            <div className="dashboard">
                <Grid container>
                    <Grid item xs={6}>
                        <div id="xyChart" style={{ height: "500px" }} />
                    </Grid>
                    <Grid item container xs={6}>
                        <List>
                            <ListItem>
                                <ListItemText
                                    primary={
                                        <Typography component="span">
                                            Всего задач за отчётный период: <b>{chartData.total}</b>
                                        </Typography>
                                    }
                                />
                            </ListItem>
                            <ListItem>
                                <ListItemText
                                    primary={
                                        <Typography component="span">
                                            Максимальная загруженность:{" "}
                                            <b>
                                                {chartData.data[0]?.name}, {chartData.data[0]?.percent * 10}
                                            </b>{" "}
                                            {getPrulal(chartData.data[0]?.percent * 10)} на проект
                                        </Typography>
                                    }
                                />
                            </ListItem>
                            <ListItem>
                                <ListItemText
                                    primary={
                                        <Typography component="span">
                                            Среднее количество задач на проект:
                                            <b>{Math.round(chartData.total / totalTasks)}</b>
                                        </Typography>
                                    }
                                />
                            </ListItem>
                        </List>
                    </Grid>
                </Grid>
                <Grid container>
                    <Grid item xs={12} className={classes.tableHeader}>
                        Сводка по проектам
                    </Grid>
                    <Grid item xs={12}>
                        <Paper>
                            <Table size="small" aria-label="a dense table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell className={classes.headerCell}>Проект</TableCell>
                                        <TableCell className={classes.headerCell} align="right">
                                            Сотрудники
                                        </TableCell>
                                        <TableCell className={classes.headerCell} align="right">
                                            Проведённое время
                                        </TableCell>
                                        <TableCell className={classes.headerCell} align="right">
                                            Задача завершена
                                        </TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {tableData.map(({ name, employees }: TasksProps) =>
                                        employees.length ? (
                                            <>
                                                <TableRow key={name}>
                                                    <TableCell rowSpan={employees.length + 1}>{name}</TableCell>
                                                </TableRow>
                                                {employees.map(({ completed, daysOnProject, employee }) => (
                                                    <TableRow key={name + employee}>
                                                        <TableCell align="right">{employee}</TableCell>
                                                        <TableCell align="right">{daysOnProject}</TableCell>
                                                        <TableCell align="right">{completed ? "Да" : "Нет"}</TableCell>
                                                    </TableRow>
                                                ))}
                                            </>
                                        ) : null
                                    )}
                                </TableBody>
                            </Table>
                        </Paper>
                    </Grid>
                </Grid>
            </div>
        </>
    )
})

export default EmploymentReport
