import { Dictionary } from "./../../modules/shared/models"
import { makeObservable, computed, observable } from "mobx"
import API from "../../api"

interface TasksProps {
    name: string
    departments: Array<Dictionary>
}

interface ChartDataProps {
    name: string
    tasks: number
    percent: number
}

export class EmploymentReportStore {
    reportData: Dictionary = []
    filteredData: Dictionary = []
    tableData: Dictionary = []
    filterData: Array<string> = []
    totalTasks: number = 0
    error = ""

    constructor() {
        makeObservable(this, {
            filteredData: observable.ref,
            filterData: observable.ref,
            tableData: observable.ref,
            totalTasks: observable.ref,
            error: observable.ref,
            chartData: computed,
        })
    }

    loadData = async () => {
        try {
            const employment = await API.post("/statistics/clients/employment", {})
            const tasks = await API.post("/statistics/clients/tasks", {})

            let filteredTasks: Dictionary = {},
                totalTasks = 0,
                filters: Array<string> = []
            tasks.forEach(({ departments }: TasksProps) => {
                departments.forEach(({ name, tasks }: Dictionary) => {
                    if (!filteredTasks[name]) {
                        filteredTasks[name] = {
                            name,
                            tasks: 0,
                        }
                        filters.push(name)
                    }
                    totalTasks += tasks
                    filteredTasks[name].tasks += tasks
                })
            })

            let filterToStore = Object.values(filteredTasks).map((task: Dictionary) => {
                return {
                    ...task,
                    percent: +(task.tasks / tasks.length).toFixed(1),
                }
            })

            this.reportData = filterToStore
            this.filteredData = filterToStore
            this.filterData = filters
            this.totalTasks = totalTasks
            this.tableData = employment
        } catch (e) {
            console.log("loadData e", e)
        }
    }

    get chartData(): Dictionary {
        let totalTasks = 0
        this.filteredData.forEach(({ tasks }: ChartDataProps) => {
            totalTasks += tasks
        })

        return {
            total: totalTasks,
            data: this.filteredData
                .map((department: ChartDataProps) => {
                    return {
                        ...department,
                        percent: +(department.tasks / this.totalTasks).toFixed(1),
                    }
                })
                .sort((a: ChartDataProps, b: ChartDataProps) => (a.percent < b.percent ? 1 : -1)),
        }
    }
}

export const employmentReportStore = new EmploymentReportStore()
