import React, { FC, Suspense } from "react"
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom"
import { ThemeProvider, CssBaseline, CircularProgress } from "@material-ui/core"

import { Navbar } from "./modules/shared/components"
import { HomePage, Statistics, DashboardPage } from "./pages"
import { theme } from "./modules/shared/constants"

const App: FC = () => {
    return (
        <BrowserRouter>
            <ThemeProvider theme={theme}>
                <CssBaseline />
                <Navbar />
                <Switch>
                    <Redirect exact path="/" to="/home" />
                    <Route path={["/home", "/home/:regionName"]}>
                        <Suspense fallback={<CircularProgress />}>
                            <HomePage />
                        </Suspense>
                    </Route>
                    <Route path="/statistics">
                        <Suspense fallback={<CircularProgress />}>
                            <Statistics />
                        </Suspense>
                    </Route>
                    <Route path="/dashboard">
                        <Suspense fallback={<CircularProgress />}>
                            <DashboardPage />
                        </Suspense>
                    </Route>
                </Switch>
            </ThemeProvider>
        </BrowserRouter>
    )
}

export { App }
