import Cookies from "js-cookie"

type TMethod = "GET" | "POST" | "PUT" | "DELETE"

interface Headers {
    [key: string]: string
}

type THeaders = Headers & {
    "Accept": string
    "Content-Type"?: string
    "Authorization"?: string
}

class API {
    public get(url: string) {
        return this.sendRequest("GET", url)
    }

    public post(url: string, body: { [key: string]: string | number } | FormData) {
        return this.sendRequest("POST", url, body)
    }

    public put(url: string) {
        return this.sendRequest("PUT", url)
    }

    public delete(url: string) {
        return this.sendRequest("DELETE", url)
    }

    private sendRequest = async (
        method: TMethod,
        url: string,
        body?: { [key: string]: string | number } | FormData
    ): Promise<any> => {
        const headers: THeaders =
            body && body instanceof FormData
                ? {
                      Accept: "application/json",
                  }
                : {
                      "Accept": "application/json",
                      "Content-Type": "application/json; charset=UTF-8",
                  }

        const token = Cookies.get("token")
        if (token) {
            headers.Authorization = `Bearer ${token}`
        }

        const config: {
            method: TMethod
            headers: THeaders
            body?: string | FormData
        } = {
            method,
            headers,
        }

        if (body) {
            config.body = body instanceof FormData ? body : JSON.stringify(body)
        }
        try {
            const response = await fetch("/api/v1" + url, config)
            const json = await response.json()
            return await Promise.resolve(json.data)
        } catch (error) {
            return await Promise.reject(error)
        }
    }
}

export default new API()
