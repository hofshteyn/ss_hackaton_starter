import { ChartItem } from "../models"

export const charts: ChartItem[] = [
    { name: "Гистограмма", type: "xyChart" },
    { name: "Круговая диаграмма", type: "DonutChart" },
    { name: "Многоуровневая диаграмма", type: "clusteredBar" },
]
