import React, { FC, useEffect } from "react"
import * as am4core from "@amcharts/amcharts4/core"
import * as am4charts from "@amcharts/amcharts4/charts"

interface ChartProps {
    data: any[]
    fields: string[]
    description: string
}

export const DonutChart: FC<ChartProps> = ({ data, fields, description }) => {
    useEffect(() => {
        let donutChart: any
        donutChart = am4core.create("donutChart", am4charts.PieChart)
        donutChart.innerRadius = am4core.percent(40)
        donutChart.radius = am4core.percent(60)
        donutChart.data = data
        const [category, value] = fields

        let title = donutChart.titles.create()
        title.text = `${description}\n[font-size:14px]Соотношение данных: ${category} - ${value}`
        title.fontSize = 18

        // Add and configure Series
        let pieSeries = donutChart.series.push(new am4charts.PieSeries())
        pieSeries.dataFields.value = value
        pieSeries.dataFields.category = category
        pieSeries.slices.template.stroke = am4core.color("#fff")
        pieSeries.slices.template.strokeWidth = 2
        pieSeries.slices.template.strokeOpacity = 1
        pieSeries.slices.template.tooltipText = "{category}: {value.value.formatNumber('#.00')}"
        pieSeries.labels.template.text = "{category} - {value.value.formatNumber('#.00')}"

        // This creates initial animation
        pieSeries.hiddenState.properties.opacity = 1
        pieSeries.hiddenState.properties.endAngle = -90
        pieSeries.hiddenState.properties.startAngle = -90
    }, [])

    return <div id="donutChart" style={{ height: "500px" }} />
}
