import React, { FC, useEffect } from "react"
import * as am4core from "@amcharts/amcharts4/core"
import * as am4charts from "@amcharts/amcharts4/charts"

interface ChartProps {
    data: any[]
    fields: string[]
    description: string
}

export const ClusteredChart: FC<ChartProps> = ({ data, fields, description }) => {
    useEffect(() => {
        let clusteredChart: any
        clusteredChart = am4core.create("clusteredChart", am4charts.XYChart)
        clusteredChart.hiddenState.properties.opacity = 0
        clusteredChart.data = data
        const [category] = fields

        let title = clusteredChart.titles.create()
        title.fontSize = 18
        title.marginBottom = 10
        let categoryAxis = clusteredChart.yAxes.push(new am4charts.CategoryAxis())
        categoryAxis.dataFields.category = category
        categoryAxis.renderer.grid.template.location = 0
        categoryAxis.renderer.minGridDistance = 30
        categoryAxis.title.text = description || ""
        categoryAxis.title.fontSize = 16
        categoryAxis.renderer.labels.template.dy = -10
        categoryAxis.renderer.labels.template.dx = -15

        var valueAxis = clusteredChart.xAxes.push(new am4charts.ValueAxis())
        valueAxis.renderer.opposite = true

        fields.forEach((field: string, index: number) => {
            if (index === 0) {
                return
            }
            let series = clusteredChart.series.push(new am4charts.ColumnSeries())
            series.dataFields.valueX = field
            series.dataFields.categoryY = category
            series.name = field
            series.columns.template.tooltipText = "{categoryY}: [bold]{valueX}[/]"
            series.columns.template.fillOpacity = 0.8
            series.columns.template.adapter.add("fill", function (fill: any, target: any) {
                return clusteredChart.colors.getIndex(target.dataItem.index)
            })

            var labelBullet = series.bullets.push(new am4charts.LabelBullet())
            labelBullet.label.text = "{valueX}"
            labelBullet.locationY = 0.1
            labelBullet.label.hideOversized = true

            let columnTemplate = series.columns.template
            columnTemplate.strokeWidth = 2
            columnTemplate.strokeOpacity = 1
        })
    }, [])

    return <div id="clusteredChart" style={{ height: "500px" }} />
}
