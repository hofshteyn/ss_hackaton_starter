import React, { FC, useState, useEffect } from "react"
import * as am4core from "@amcharts/amcharts4/core"
import * as am4charts from "@amcharts/amcharts4/charts"
import am4themes_animated from "@amcharts/amcharts4/themes/animated"

interface ChartProps {
    data: any[]
    fields: string[]
    description: string
    additionalText?: string
}

export const XyChart: FC<ChartProps> = ({ data, fields, description, additionalText }) => {
    useEffect(() => {
        let xyChart: any, series

        xyChart = am4core.create("xyChart", am4charts.XYChart)
        xyChart.hiddenState.properties.opacity = 0
        xyChart.data = data
        const [category, value] = fields

        let title = xyChart.titles.create()
        title.text = `${description}\n[font-size:14px]Соотношение данных: ${category} - ${value}`
        title.fontSize = 18
        title.marginBottom = 10
        let categoryAxis = xyChart.xAxes.push(new am4charts.CategoryAxis())
        categoryAxis.dataFields.category = category
        categoryAxis.renderer.grid.template.location = 0
        categoryAxis.renderer.minGridDistance = 30
        categoryAxis.title.text = additionalText || ""
        categoryAxis.title.fontSize = 16
        categoryAxis.renderer.labels.template.rotation = -90
        categoryAxis.renderer.labels.template.dy = -10
        categoryAxis.renderer.labels.template.dx = -15

        let valueAxis = xyChart.yAxes.push(new am4charts.ValueAxis())

        series = xyChart.series.push(new am4charts.ColumnSeries())
        series.dataFields.valueY = value
        series.dataFields.categoryX = category
        series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]"
        series.columns.template.fillOpacity = 0.8
        series.columns.template.adapter.add("fill", function (fill: any, target: any) {
            return xyChart.colors.getIndex(target.dataItem.index)
        })

        var labelBullet = series.bullets.push(new am4charts.LabelBullet())
        labelBullet.label.text = "{valueY}"
        labelBullet.locationY = 0.1
        labelBullet.label.hideOversized = true

        let columnTemplate = series.columns.template
        columnTemplate.strokeWidth = 2
        columnTemplate.strokeOpacity = 1
    }, [])

    return <div id="xyChart" style={{ height: "500px" }} />
}
