import { action, computed, makeObservable, observable } from "mobx"
import { Map as MapboxMap, LngLatBounds, LngLatLike } from "mapbox-gl"
import { Feature, Geometry, MultiPolygon } from "geojson"

import { regionSource } from "../constants/sources"

const getArrayDepth = (value: any[]): number => {
    return Array.isArray(value) ? 1 + Math.max(...value.map(getArrayDepth)) : 0
}

export class MapStore {
    map: MapboxMap | null = null

    constructor() {
        makeObservable(this, {
            map: observable,

            setMap: action,
            selectRegion: action,
        })
    }

    setMap(map: MapboxMap) {
        this.map = map
    }

    selectRegion(regionName: string) {
        const region: Feature<Geometry, { [name: string]: any }> | undefined = regionSource.features.find(
            (feature: Feature) => feature.properties?.label === regionName
        )
        if (region) {
            const geometry: any = region.geometry
            const coordinates = geometry.coordinates
            const flattenCoord = coordinates.flat(getArrayDepth(coordinates) - 2) as unknown as LngLatLike[]
            const bounds = new LngLatBounds(flattenCoord[0], flattenCoord[1])
            for (const coord of flattenCoord) {
                bounds.extend(coord)
            }

            this.map?.fitBounds(bounds, {
                padding: 20,
            })
        }
    }
}

export const mapStore = new MapStore()
