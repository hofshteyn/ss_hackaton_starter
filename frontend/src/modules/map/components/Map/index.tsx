import { Map as MapboxMap, MapboxOptions, LngLatBounds } from "mapbox-gl"
import React, { FC, useRef, useState, useEffect } from "react"
import { observer } from "mobx-react"
import { makeStyles } from "@material-ui/core"

import { mapStore } from "../../store"
import { getFillLayer, getOutlineLayer } from "../../constants/layers"
import { regionSource } from "../../constants/sources"

const defaultOptions: MapboxOptions = {
    container: null as unknown as string,
    zoom: 3.5,
    minZoom: 1,
    dragRotate: false,
    scrollZoom: true,
    center: { lat: 0, lng: 0 },
    style: "mapbox://styles/mapbox/light-v10",
    accessToken: "pk.eyJ1IjoiaWx5YS1nb2ZzaHRleW4iLCJhIjoiY2s4Zno3NGVyMDMycDNnbzF6YWYzbWhkeSJ9.MIskq-UFcuvIPMWFDt1ntw",
}

const useStyles = makeStyles(() => ({
    mapContainer: {
        position: "relative",
        height: "100%",
    },
    map: {
        "position": "absolute",
        "zIndex": 1,
        "top": "64px",
        "right": 0,
        "width": "100%",
        "height": "100%",
        ".mapboxgl-canvas-container": {
            ".mapboxgl-canvas:focus": {
                outline: "none",
            },
        },
    },
}))

export const Map: FC<{ onSelectRegion: (regionName: string) => void }> = observer(({ onSelectRegion }) => {
    const classes = useStyles()
    const mapContainerRef = useRef(null)

    useEffect(() => {
        const map = new MapboxMap({
            ...defaultOptions,
            container: mapContainerRef.current as unknown as HTMLElement,
        })

        map.on("load", () => {
            mapStore.setMap(map)
            map.addSource("region", {
                type: "geojson",
                data: regionSource,
            })

            map.addLayer(getFillLayer("region"))
            map.addLayer(getOutlineLayer("region"))
            map.flyTo({ center: [37.618423, 55.751244], speed: 0.65, essential: true })
        })

        map.on("click", (e: any) => {
            const features = map.queryRenderedFeatures(e.point)
            if (features.length) {
                const selectedFeature = features.find((feature) => feature.source === "region")
                if (selectedFeature) {
                    selectedFeature.properties.selected = true;
                    onSelectRegion(selectedFeature.properties?.label)
                }
            }
        })

        return () => map.remove()
    }, [])
    return (
        <div className={classes.mapContainer}>
            <div ref={mapContainerRef} className={classes.map}></div>
        </div>
    )
})
