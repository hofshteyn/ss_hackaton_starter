import { AnyLayer } from "mapbox-gl"

export const getFillLayer = (id: string): AnyLayer => ({
    id: `${id}-fill`,
    type: "fill",
    source: id,
    layout: {},
    paint: {
        "fill-color": "#184faa",
        "fill-opacity": ["match", ["get", "selected"], "true", 0.5, 0.15],
    },
})

export const getOutlineLayer = (id: string): AnyLayer => ({
    id: `${id}-outline`,
    type: "line",
    source: id,
    layout: {},
    paint: {
        "line-color": "#184faa",
        "line-width": 2,
    },
})
