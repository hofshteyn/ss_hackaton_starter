import { Feature, MultiPolygon, Polygon } from "geojson"

import kazan from "./kazan"
import krasnodar from "./krasnodarskiy-krai"
import saintPetersburg from "./saintPetersburg"
import samara from "./samara"
import saransk from "./saransk"
import uljanovsk from "./uljanovsk"

export const regionSources: Feature<MultiPolygon | Polygon>[] = [kazan, krasnodar, saintPetersburg, samara, saransk, uljanovsk]
