import { regionSources } from "../geojson/index"
import { FeatureCollection } from "geojson"

export const regionSource: FeatureCollection = {
	type: "FeatureCollection",
	features: regionSources,
}
