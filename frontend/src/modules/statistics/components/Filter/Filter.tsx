import React, { useEffect, useMemo } from "react"
import { observer } from "mobx-react"
import { IconButton, Slide } from "@material-ui/core"
import { LockOpen, LockOutlined } from "@material-ui/icons"
import { SelectMenu, RadioSelect } from ".."
import { period } from "../../../../../mocks/DeltaTime"
import { Dictionary } from "../../../shared/models"

import { filterStore } from "./FilterStore"

interface iFilterProps {
    departments: Array<string>
    statuses: Array<string>
    // period: Dictionary
}

const Filter = observer(({ departments, statuses }: iFilterProps) => {
    useEffect(() => {
        const initFilters: Dictionary = {}
        if (departments.length) {
            initFilters.departments = departments
        }
        if (statuses.length) {
            initFilters.statuses = statuses
        }
        filterStore.init(initFilters)
    }, [departments, statuses])

    const showItems = useMemo(() => {
        return {
            departments: !!departments.length,
            statuses: !!statuses.length,
        }
    }, [departments, statuses])

    const filterVisible = filterStore.filterVisible
    const filterLocked = filterStore.filterLocked
    const handleLockFilter = () => filterStore.lockFilter()
    const showFilter = () => filterStore.changeFilter(true)
    const hideFilter = () => filterStore.changeFilter(false)
    const updateFilters = (field: string, filters: Array<string>) => {
        filterStore.updateFilters(field, filters)
    }
    const updatePeriod = (field: string, value: string) => {
        // const valueToSet = value === "all" ? null : period.find((item) => item.label === "Год").offset
        // filterStore.updatePeriod(field, valueToSet)
    }

    return (
        <div onMouseEnter={showFilter} onMouseLeave={hideFilter}>
            <Slide direction="down" in={filterLocked || filterVisible} timeout={500}>
                <div className="filter">
                    <IconButton onClick={handleLockFilter}>{filterLocked ? <LockOutlined /> : <LockOpen />}</IconButton>
                    {showItems.departments && (
                        <SelectMenu
                            store={"departments"}
                            label={"Отделы"}
                            items={departments}
                            onClose={updateFilters}
                        />
                    )}
                    {showItems.statuses && (
                        <SelectMenu store={"statuses"} label={"Статусы"} items={statuses} onClose={updateFilters} />
                    )}
                    <RadioSelect
                        store={"period"}
                        label={"Период"}
                        items={period.map(({ label }) => label)}
                        onClose={updatePeriod}
                    />
                </div>
            </Slide>
        </div>
    )
})

export default Filter
