import { Dictionary } from "../../../shared/models"
import { makeObservable, computed, action, observable, runInAction } from "mobx"
import { storage } from "../../../../../service/storage"

export class FilterStore {
    filterLocked: boolean = true
    filterVisible: boolean = false
    filters: Dictionary = {}
    error = ""

    constructor() {
        makeObservable(this, {
            filterVisible: observable.ref,
            filterLocked: observable.ref,
            filters: observable.ref,
            error: observable.ref,

            changeFilter: action,
            lockFilter: action,
            updateFilters: action,
            updatePeriod: action,
            getFilters: computed,
            init: action,
        })
    }

    init(filters: Dictionary): void {
        try {
            if (storage.filterSettings) {
                this.filterLocked = storage.filterSettings
            }
            this.filters = filters
        } catch (e: any) {
            runInAction(() => {
                this.error = e
            })
        }
    }

    lockFilter(): void {
        try {
            this.filterLocked = !this.filterLocked
            storage.saveFilter(this.filterLocked)
        } catch (e: any) {
            runInAction(() => {
                this.error = e
            })
        }
    }

    changeFilter(value: boolean): void {
        try {
            this.filterVisible = value
        } catch (e: any) {
            runInAction(() => {
                this.error = e
            })
        }
    }

    updateFilters(field: string, value: Array<string>): void {
        try {
            this.filters = { ...this.filters, [field]: value }
        } catch (e: any) {
            runInAction(() => {
                this.error = e
            })
        }
    }

    updatePeriod(field: string, value: number | null): void {
        try {
            this.filters = { ...this.filters, [field]: value }
        } catch (e: any) {
            runInAction(() => {
                this.error = e
            })
        }
    }

    get getFilters(): Dictionary {
        return this.filters
    }
}

export const filterStore = new FilterStore()
