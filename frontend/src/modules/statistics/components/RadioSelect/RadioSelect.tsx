import React, { useState } from "react"
import { Radio, RadioGroup, FormControlLabel, FormControl, Menu, Button } from "@material-ui/core"
import { makeStyles, useTheme } from "@material-ui/core/styles"

type Props = {
    store: string
    label: string
    items: Array<string>
    onClose: any
}
const useStyles = makeStyles((theme) => ({
    RadioItem: {
        marginLeft: 0,
    },
}))

const RadioSelect = ({ label, items, store, onClose }: Props) => {
    const classes = useStyles()
    const [anchorEl, setAnchorEl] = useState(null)
    const openMenu = (event: any) => {
        setAnchorEl(event.currentTarget)
    }
    const closeMenu = () => {
        setAnchorEl(null)
        onClose(store, value)
    }
    const [value, setValue] = React.useState("all")

    const handleChange = (event: any) => {
        setValue(event.target.value)
    }

    const renderRadioItem = () => {
        return items.map((value) => {
            return (
                <FormControlLabel
                    className={classes.RadioItem}
                    key={value}
                    value={value}
                    control={<Radio />}
                    label={value}
                />
            )
        })
    }

    return (
        <>
            <Button aria-controls="simple-menu" aria-haspopup="true" onClick={openMenu}>
                {label}
            </Button>
            <Menu id="simple-menu" anchorEl={anchorEl} keepMounted open={Boolean(anchorEl)} onClose={closeMenu}>
                <FormControl component="fieldset">
                    <RadioGroup aria-label="gender" name="gender1" value={value} onChange={handleChange}>
                        <FormControlLabel
                            className={classes.RadioItem}
                            value={"all"}
                            control={<Radio />}
                            label={"Все"}
                        />
                        {renderRadioItem()}
                    </RadioGroup>
                </FormControl>
            </Menu>
        </>
    )
}

export default RadioSelect
