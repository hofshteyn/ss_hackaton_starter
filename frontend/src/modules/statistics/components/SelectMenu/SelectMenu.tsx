import React, { useState } from "react"

import { List, ListItem, ListItemText, Button, Checkbox, Menu } from "@material-ui/core"

type Props = {
    store: string
    label: string
    items: Array<string>
    onClose: any
}

export const SelectMenu = ({ store, label, items, onClose }: Props) => {
    const [anchorEl, setAnchorEl] = useState(null)
    const openMenu = (event: any) => {
        setAnchorEl(event.currentTarget)
    }
    const closeMenu = () => {
        setAnchorEl(null)
        onClose(store, checked)
    }

    const [checked, setChecked] = useState(items)
    const [checkboxItems, setCheckboxItems] = useState(items)

    const not = (a: Array<any>, b: Array<any>) => {
        return a.filter((value: any) => b.indexOf(value) === -1)
    }

    const intersection = (a: Array<any>, b: Array<any>) => {
        return a.filter((value: any) => b.indexOf(value) !== -1)
    }

    const union = (a: Array<any>, b: Array<any>) => {
        return [...a, ...not(b, a)]
    }

    const numberOfChecked = (items: Array<any>) => intersection(checked, items).length

    const handleToggleAll = (items: Array<any>) => () => {
        if (numberOfChecked(items) === items.length) {
            setChecked(not(checked, items))
        } else {
            setChecked(union(checked, items))
        }
    }

    const handleToggle = (value: any) => () => {
        const currentIndex = checked.indexOf(value)
        const newChecked = [...checked]

        if (currentIndex === -1) {
            newChecked.push(value)
        } else {
            newChecked.splice(currentIndex, 1)
        }
        setChecked(newChecked)
    }

    const renderCheckboxes = () => {
        return checkboxItems.map((value) => {
            return (
                <ListItem key={value} role="listitem" button onClick={handleToggle(value)}>
                    <Checkbox
                        color="primary"
                        checked={checked.indexOf(value) !== -1}
                        tabIndex={-1}
                        disableRipple
                        inputProps={{ "aria-labelledby": value }}
                    />
                    <ListItemText primary={`${value}`} />
                </ListItem>
            )
        })
    }

    return (
        <>
            <Button aria-controls="simple-menu" aria-haspopup="true" onClick={openMenu}>
                {label}
            </Button>
            <Menu id="simple-menu" anchorEl={anchorEl} keepMounted open={Boolean(anchorEl)} onClose={closeMenu}>
                <List dense component="div" role="list">
                    <ListItem role="listitem" button onClick={handleToggleAll(checkboxItems)}>
                        <Checkbox
                            color="primary"
                            onClick={handleToggleAll(checkboxItems)}
                            checked={
                                numberOfChecked(checkboxItems) === checkboxItems.length && checkboxItems.length !== 0
                            }
                            indeterminate={
                                numberOfChecked(checkboxItems) !== checkboxItems.length &&
                                numberOfChecked(checkboxItems) !== 0
                            }
                            disabled={checkboxItems.length === 0}
                            inputProps={{ "aria-label": "all items selected" }}
                        />
                        <ListItemText primary={`Все`} />
                    </ListItem>
                    {renderCheckboxes()}
                </List>
            </Menu>
        </>
    )
}
