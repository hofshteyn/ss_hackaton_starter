import React, { useState } from "react"
import { useHistory } from "react-router-dom"
import clsx from "clsx"
import { makeStyles } from "@material-ui/core/styles"
import { Drawer, List, IconButton, ListItem, ListItemIcon, ListItemText, Divider } from "@material-ui/core"
import { AccountBalance, ChevronLeft, ChevronRight, Person, AssignmentIndOutlined } from "@material-ui/icons"

import { observer } from "mobx-react"

import { sidebarStore } from "./SidebarStore"

const useStyles = makeStyles((theme) => ({
    drawer: {
        flexShrink: 0,
        whiteSpace: "nowrap",
    },
    drawerPaper: {
        height: "calc(100vh - 64px)",
        top: "67px",
        background: theme.palette.background.default,
    },
    drawerOpen: {
        width: 240,
        transition: theme.transitions.create("width", {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerClose: {
        transition: theme.transitions.create("width", {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        overflowX: "hidden",
        width: theme.spacing(7) + 1,
        [theme.breakpoints.up("sm")]: {
            width: theme.spacing(9) + 1,
        },
    },
    toolbar: {
        display: "flex",
        alignItems: "center",
        justifyContent: "flex-end",
        padding: theme.spacing(0, 1),
        ...theme.mixins.toolbar,
    },
    activeIcon: {
        color: theme.palette.primary.main,
    },
}))

const Sidebar = observer(() => {
    const classes = useStyles()
    const history = useHistory()
    const [activeItem, setActiveItem] = useState(window.location.pathname.replace("/statistics", "") || "/deltaTime")
    const drawerOpen = sidebarStore.drawerOpen
    const handleDrawer = () => sidebarStore.changeDrawer()

    const goTo = (route: string) => () => {
        history.push(`/statistics/${route}`)
        setActiveItem(`/${route}`)
    }

    return (
        <Drawer
            variant="permanent"
            className={clsx(classes.drawer, {
                [classes.drawerOpen]: drawerOpen,
                [classes.drawerClose]: !drawerOpen,
            })}
            classes={{
                paper: clsx(classes.drawerPaper, {
                    [classes.drawerOpen]: drawerOpen,
                    [classes.drawerClose]: !drawerOpen,
                }),
            }}
        >
            <div className={classes.toolbar}>
                <IconButton onClick={handleDrawer}>{drawerOpen ? <ChevronLeft /> : <ChevronRight />}</IconButton>
            </div>
            <Divider />
            <List>
                <ListItem button onClick={goTo("deltaTime")}>
                    <ListItemIcon
                        className={clsx({
                            [classes.activeIcon]: activeItem === "/deltaTime",
                        })}
                    >
                        <AccountBalance />
                    </ListItemIcon>
                    <ListItemText
                        className={clsx({
                            [classes.activeIcon]: activeItem === "/deltaTime",
                        })}
                        primary={"Отделы"}
                    />
                </ListItem>
                <ListItem button onClick={goTo("taskType")}>
                    <ListItemIcon
                        className={clsx({
                            [classes.activeIcon]: activeItem === "/taskType",
                        })}
                    >
                        <Person />
                    </ListItemIcon>
                    <ListItemText
                        className={clsx({
                            [classes.activeIcon]: activeItem === "/taskType",
                        })}
                        primary={"Занятость"}
                    />
                </ListItem>
                <ListItem button onClick={goTo("employment")}>
                    <ListItemIcon
                        className={clsx({
                            [classes.activeIcon]: activeItem === "/employment",
                        })}
                    >
                        <AssignmentIndOutlined />
                    </ListItemIcon>
                    <ListItemText
                        className={clsx({
                            [classes.activeIcon]: activeItem === "/employment",
                        })}
                        primary={"Ком. занятость"}
                    />
                </ListItem>
            </List>
        </Drawer>
    )
})

export default Sidebar
