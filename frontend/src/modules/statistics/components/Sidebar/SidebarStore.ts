import { makeObservable, computed, action, observable, runInAction } from "mobx"
import { storage } from "../../../../../service/storage"

export class SidebarStore {
    drawerOpen: boolean = false
    error = ""

    constructor() {
        makeObservable(this, {
            drawerOpen: observable.ref,
            error: observable.ref,

            changeDrawer: action,
        })

        this.init()
    }

    init(): void {
        try {
            if (storage.drawerSettings) {
                this.drawerOpen = storage.drawerSettings
            }
        } catch (e: any) {
            runInAction(() => {
                this.error = e
            })
        }
    }

    changeDrawer(): void {
        try {
            this.drawerOpen = !this.drawerOpen
            storage.saveDrawer(this.drawerOpen)
        } catch (e: any) {
            runInAction(() => {
                this.error = e
            })
        }
    }
}

export const sidebarStore = new SidebarStore()
