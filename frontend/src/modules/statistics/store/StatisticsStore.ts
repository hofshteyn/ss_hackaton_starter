import { computed, action, observable, makeObservable } from "mobx"

import { CommonStatistics } from "../models/CommonStatistics"
import { LoadingStatuses, Dictionary } from "../../shared/models"
import API from "../../../api"
import { offices } from "../../shared/constants"

class StatisticsStore {
    commonStatistics: CommonStatistics[] = []
    status: LoadingStatuses = LoadingStatuses.empty
    averageRemoteAge = 0

    constructor() {
        makeObservable(this, {
            commonStatistics: observable,
            averageRemoteAge: observable.ref,

            setStatistics: action,
            countRemoteAverageAge: action,

            statisticByRegion: computed,
        })
    }

    loadCommonStatistics = async () => {
        try {
            this.setStatus(LoadingStatuses.pending)
            const statistics = await API.get("/statistics/regions")
            this.setStatistics(statistics)
            this.countRemoteAverageAge(statistics)
            this.setStatus(LoadingStatuses.success)
        } catch (e) {
            this.setStatus(LoadingStatuses.error)
        }
    }

    setStatistics(data: CommonStatistics[]) {
        this.commonStatistics = data
    }

    setStatus(status: LoadingStatuses) {
        this.status = status
    }

    countRemoteAverageAge(statistics: CommonStatistics[]): void {
        let quantity = 0
        let ageSummary = 0
        for (const item of statistics) {
            if (!offices.includes(item.name)) {
                quantity++
                ageSummary += item.avrAge
            }
        }
        this.averageRemoteAge = Math.ceil(((ageSummary / quantity) * 10) / 10)
    }

    get statisticByRegion(): Dictionary {
        const statByRegions: CommonStatistics[] = []
        let quantity = 0
        let ageSummary = 0
        let totalEmployees = 0
        for (let stat of this?.commonStatistics) {
            quantity++
            ageSummary += stat.avrAge
            totalEmployees += stat.employees
            if (offices.includes(stat.name)) {
                statByRegions.push(stat)
            } else {
                const remoteStat = statByRegions.find((s) => s.name === "Удаленные сотрудники")
                if (remoteStat) {
                    remoteStat.employees += stat.employees
                } else {
                    statByRegions.push({
                        id: "0",
                        name: "Удаленные сотрудники",
                        employees: stat.employees,
                        avrAge: this.averageRemoteAge,
                    })
                }
            }
        }
        return {
            regions: statByRegions.sort((a, b) => a.name.localeCompare(b.name)),
            totalAverageAge: Math.ceil(((ageSummary / quantity) * 10) / 10),
            totalEmployees,
        }
    }
}

export const statisticsStore = new StatisticsStore()
