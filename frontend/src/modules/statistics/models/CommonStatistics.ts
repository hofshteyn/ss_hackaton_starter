export interface CommonStatistics {
    id: string
    name: string
    employees: number
    avrAge: number
}
