import React, { FC, useState } from "react"
import { AppBar, Toolbar, IconButton, Button, makeStyles } from "@material-ui/core"
import { useHistory } from "react-router-dom"

import MenuIcon from "@material-ui/icons/Menu"

interface RouteItem {
    name: string
    route: string
}

const routes: RouteItem[] = [
    { name: "Карта", route: "home" },
    { name: "Статистика", route: "statistics" },
    { name: "Конструктор", route: "dashboard" },
]

const useStyles = makeStyles((theme) => ({
    root: {
        background: theme.palette.background.default,
    },
    navButton: {
        height: "64px",
        padding: "32px",
        borderRadius: "0px",
        fontSize: "17px",
    },
    navButtonSelected: {
        borderBottom: `3px solid ${theme.palette.primary.main}`,
    },
}))

export const Navbar: FC = () => {
    const classes = useStyles()
    const history = useHistory()
    const initIndex = routes.findIndex((item: RouteItem) => history.location.pathname.includes(item.route))
    const [activeTabIndex, setActiveTabIndex] = useState(initIndex)
    return (
        <AppBar position="absolute" className={classes.root}>
            <Toolbar>
                <IconButton edge="start" className="" color="primary" aria-label="menu">
                    <MenuIcon />
                </IconButton>

                {routes.map((item: RouteItem, index: number) => (
                    <Button
                        key={index}
                        color="primary"
                        className={`${classes.navButton} ${index === activeTabIndex ? classes.navButtonSelected : ""}`}
                        onClick={() => {
                            setActiveTabIndex(index)
                            history.push(`/${item.route}`)
                        }}
                    >
                        {item.name}
                    </Button>
                ))}
            </Toolbar>
        </AppBar>
    )
}
