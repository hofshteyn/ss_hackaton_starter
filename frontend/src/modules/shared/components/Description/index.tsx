import React from "react"
import { Grid, makeStyles, Divider } from "@material-ui/core"

interface DescriptionProps {
    header: string
    description: string
}

const useStyles = makeStyles({
    root: {
        margin: "8px 0",
    },
    header: {
        fontSize: "24px",
        fontWeight: "bold",
    },
    description: {
        fontSize: "16px",
    },
    divider: {
        marginBottom: "16px",
    },
})

export const Description = ({ header, description }: DescriptionProps) => {
    const classes = useStyles()
    return (
        <>
            <Grid container direction="column" justifyContent="center" alignItems="center" className={classes.root}>
                <Grid item xs={12} className={classes.header}>
                    {header}
                </Grid>
                <Grid item xs={12} className={classes.description}>
                    {description}
                </Grid>
            </Grid>
            <Divider className={classes.divider} />
        </>
    )
}
