import { Grid, List, ListItem, ListItemText, makeStyles } from "@material-ui/core"
import React, { FC } from "react"

const useTotalStyles = makeStyles((theme) => ({
    rightContainer: {
        maxWidth: 220,
        position: "absolute",
        zIndex: 2,
        top: "84px",
        right: "20px",
    },
    rightContent: {
        backgroundColor: theme.palette.background.paper,
        overflowY: "auto",
    },
}))

interface Props {
    employees: number
    averageAge: number
}

export const TotalList: FC<Props> = ({ employees, averageAge }: any) => {
    const classes = useTotalStyles()
    return (
        <Grid container className={classes.rightContainer}>
            <Grid item xs={12}>
                <List className={classes.rightContent}>
                    <ListItem>
                        <ListItemText primary={`Всего сотрудников: ${employees}`}></ListItemText>
                    </ListItem>
                    <ListItem>
                        <ListItemText primary={`Средний возраст: ${averageAge || 0}`}></ListItemText>
                    </ListItem>
                </List>
            </Grid>
        </Grid>
    )
}
