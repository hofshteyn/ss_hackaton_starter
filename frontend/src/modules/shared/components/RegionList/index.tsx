import React, { FC, useState } from "react"
import { Grid, makeStyles, ListItemText, List, ListItem, Collapse } from "@material-ui/core"
import ExpandLess from "@material-ui/icons/ExpandLess"
import ExpandMore from "@material-ui/icons/ExpandMore"

import { CommonStatistics } from "../../../statistics/models/CommonStatistics"

const useStyles = makeStyles((theme) => ({
    root: {
        backgroundColor: theme.palette.background.paper,
        maxHeight: "calc(100vh - 104px)",
        overflowY: "auto",
    },
    container: {
        maxWidth: 320,
        position: "absolute",
        zIndex: 2,
        top: "84px",
        left: "20px",
    },
    nested: {
        paddingLeft: theme.spacing(4),
    },
    subitem: {
        fontSize: 14,
    },
}))

interface Props {
    regions: CommonStatistics[]
    onSelectRegion: (item: string) => void
}

export const RegionList: FC<Props> = ({ regions, onSelectRegion }) => {
    const classes = useStyles()
    const [selectedItemIndex, selectItemIndex] = useState<number | undefined>(undefined)

    const onSelect = (index: number) => {
        selectItemIndex(index)
        onSelectRegion(regions[index].name)
    }

    return (
        <Grid container className={classes.container}>
            <Grid item xs={12}>
                <List component="nav" className={classes.root}>
                    {regions.map((region: CommonStatistics, index: number) => {
                        return (
                            <React.Fragment key={index}>
                                <ListItem
                                    button
                                    divider
                                    onClick={() => {
                                        onSelect(index)
                                    }}
                                >
                                    <ListItemText primary={region.name} />
                                    {selectedItemIndex === index ? <ExpandLess /> : <ExpandMore />}
                                </ListItem>
                                <Collapse in={selectedItemIndex === index} timeout="auto" unmountOnExit>
                                    <List component="div" disablePadding>
                                        <ListItem button className={classes.nested}>
                                            <ListItemText
                                                disableTypography
                                                style={{ fontSize: "14px" }}
                                                primary={`Средний возраст: ${region.avrAge}`}
                                            />
                                        </ListItem>
                                        <ListItem button className={classes.nested}>
                                            <ListItemText
                                                disableTypography
                                                style={{ fontSize: "14px" }}
                                                primary={`Количество: ${region.employees}`}
                                            />
                                        </ListItem>
                                    </List>
                                </Collapse>
                            </React.Fragment>
                        )
                    })}
                </List>
            </Grid>
        </Grid>
    )
}
