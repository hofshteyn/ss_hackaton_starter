export const colors = {
	bgColor: "#f5f9fc",
	primary: "#184faa",
	textColor: "rgba(0,0,0,.87)",
}
