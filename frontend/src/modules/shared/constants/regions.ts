export interface RegionItem {
    name: string
    age: number
    quantity: number
    experience: number
    selected: boolean
}

export const offices = [
    "Ульяновская область",
    "Краснодарский край",
    "Республика Мордовия",
    "Республика Татарстан",
    "Самарская область",
    "Самарская область",
    "Ленинградская область",
    "Удаленные сотрудники",
]

export const regions: RegionItem[] = [
    {
        name: "Ульяновская область",
        age: null,
        quantity: null,
        experience: 2.83,
        selected: false,
    },
    {
        name: "Краснодарский край",
        age: 30.49,
        quantity: 191,
        experience: 0.81,
        selected: false,
    },
    {
        name: "Республика Мордовия",
        age: 30.3,
        quantity: 79,
        experience: 0.76,
        selected: false,
    },
    {
        name: "Республика Татарстан",
        age: 28.52,
        quantity: 107,
        experience: 1.68,
        selected: false,
    },
    {
        name: "Самарская область",
        age: 27.69,
        quantity: 125,
        experience: 1.16,
        selected: false,
    },
    {
        name: "Ленинградская область",
        age: 28.27,
        quantity: 223,
        experience: 1.06,
        selected: false,
    },

    {
        name: "Удаленные сотрудники",
        age: 30.19,
        quantity: 31,
        experience: 2.58,
        selected: false,
    },
]
