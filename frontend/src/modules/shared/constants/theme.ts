import { createTheme } from "@material-ui/core/styles"
import red from "@material-ui/core/colors/red"

import { colors } from "./colors"

export const theme = createTheme({
	palette: {
		primary: {
			main: colors.primary,
		},
		secondary: {
			main: colors.primary,
		},
		error: {
			main: red.A400,
		},
		background: {
			default: colors.bgColor,
		},
		text: {
			primary: colors.textColor,
		},
	},
})
