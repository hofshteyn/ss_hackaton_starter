export enum LoadingStatuses {
    empty = "empty",
    pending = "pending",
    error = "error",
    success = "success",
}
