import { makeObservable, computed, action, observable, runInAction } from "mobx"
// ms to days: 1000*60*60*24

export class Storage {
    drawerSettings: any = null
    filterSettings: any = null
    error = ""

    constructor() {
        makeObservable(this, {
            drawerSettings: observable.ref,
            filterSettings: observable.ref,

            saveDrawer: action,
            saveFilter: action,
        })

        this.loadSetting()
    }

    loadSetting(): void {
        try {
            const drawer = localStorage.getItem("drawerSettings")
            const filter = localStorage.getItem("filterSettings")
            const parsedDrawer = drawer && JSON.parse(drawer)
            const parsedFilter = filter && JSON.parse(filter)

            this.drawerSettings = parsedDrawer
            this.filterSettings = parsedFilter
        } catch (e: any) {
            runInAction(() => {
                this.error = e
            })
        }
    }

    saveDrawer(value: boolean): void {
        try {
            localStorage.setItem("drawerSettings", `${value}`)
        } catch (e: any) {
            runInAction(() => {
                this.error = e
            })
        }
    }

    saveFilter(value: boolean): void {
        try {
            localStorage.setItem("filterSettings", `${value}`)
        } catch (e: any) {
            runInAction(() => {
                this.error = e
            })
        }
    }
}

export const storage = new Storage()
