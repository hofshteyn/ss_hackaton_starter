const deps = [
	{
		id: "58f83b3a-44d6-4b54-aab8-396bb4962deb",
		name: "Направление Mobile",
	},
	{ id: "2d9d09bc-1fa9-481f-9ba8-4adefaa299f1", name: "Лидеры команды" },
	{
		id: "bcecc271-2c45-4172-97c9-251acc7b0e88",
		name: "Направление Web-solutions",
	},
	{ id: "6589d858-90f1-462a-803b-4774dcd07dde", name: "Направление Web" },
	{
		id: "2d4608fe-464f-4017-b52b-7bacff570f83",
		name: "Направление Backend",
	},
	{
		id: "446ccdd1-e0c3-4f86-bd2c-9297b59926e0",
		name: "Направление .Net",
	},
	{
		id: "905d6468-8c2a-4272-8f73-f8d037a3be06",
		name: "Направление Frontend",
	},
	{
		id: "616a61c1-7f87-4021-94c2-1ea346e91b6c",
		name: "Направление Java",
	},
	{
		id: "3ff5af4c-f7db-47e3-9b70-b5988215c0f2",
		name: "Направление Analytics",
	},
	{ id: "6818b0ec-b26a-46de-a8b4-cd1f78150682", name: "HR" },
	{
		id: "e514e6e2-ee0c-43bd-9e36-0906b75cc023",
		name: "Группа организации мероприятий",
	},
	{ id: "aedfb301-9abd-4d56-b93b-9695763422e3", name: "Стрим - Пенза" },
	{ id: "fbcd80ab-13b6-4654-913b-71d52a7353f2", name: "Направление QA" },
	{
		id: "6cb16159-5b75-4813-9faf-4fe98ab5b8dc",
		name: "Руководители Отделов",
	},
	{ id: "e083d529-ae28-4458-b1bd-e4121551d26c", name: "Направление C++" },
	{
		id: "012298c0-4702-400c-a154-abe5455dd01d",
		name: "Направление SDET",
	},
	{
		id: "18113c7b-8c7a-4277-8b23-59eec16cebba",
		name: "Направление GoLang",
	},
	{
		id: "dacb5d6b-66a6-4d93-9d4d-ff69f7563761",
		name: "Группа Office Support",
	},
	{
		id: "37e0e158-16b6-4610-b82a-18fb3a7fb9a1",
		name: "Направление бизнес-решений",
	},
	{
		id: "4c36ded6-49b8-4807-bed0-652a3ac3c311",
		name: "Направление DevOps",
	},
	{ id: "97165752-f27b-4b8b-b81a-4087faefe8e6", name: "Направление PM" },
	{ id: "7c7ebab4-34c9-453e-9ab3-defb822c07e1", name: "Направление PHP" },
	{ id: "10ec0abd-f654-4ef4-8114-f12c213ebcce", name: "Направление PR" },
	{ id: "fd0f9b61-eca5-4d92-9213-74efafc8fa67", name: "Топ менеджеры" },
	{ id: "91ba44fc-5a3f-47fb-9fb0-85cfbe52b6e0", name: "IT Support" },
	{
		id: "d6bc0f1a-39c1-4993-9a02-b44c5a874a03",
		name: "Направление Python",
	},
	{
		id: "9c6bbace-a93c-4d22-bac5-4def9fe10fa3",
		name: "Направление Accounting",
	},
	{ id: "9b0031a4-3b30-4e2c-af6c-f5e896f7861f", name: "Отдел продаж" },
	{
		id: "9a5b1d37-40c3-4888-87a6-cf88ba805ec6",
		name: "Руководители направлений",
	},
	{
		id: "5bc13ebf-14b7-4d8c-a192-e51c839ca0bc",
		name: "Юридическое направление",
	},
	{ id: "c805a5e1-93c7-4428-929d-10a9da8703cf", name: "Стрим - Саратов" },
	{
		id: "7b23dcd5-a8f5-41c8-b529-70a9ef3f49b0",
		name: "Лидеры команд (потенциальные)",
	},
	{
		id: "ddbbca96-2318-4daa-b39e-1290155e406e",
		name: "Направление сопровождения бизнес-процессов",
	},
	{
		id: "94e507ae-353a-4fc4-86b5-c075cd250a0b",
		name: "Стрим - Нижний Новгород",
	},
	{
		id: "b8a92d4a-3b4d-4940-b477-a2bb028d3d69",
		name: "Направление внутренних проектов и автоматизации процессов",
	},
	{
		id: "83783aa3-a31f-4de3-a4b7-9608114300e7",
		name: "Направление Design",
	},
	{
		id: "c4e7a99c-b507-4b51-9eab-5c79e4b329c3",
		name: "Финансовое направление",
	},
	{
		id: "dac8bf5c-0f50-43f4-b8a6-8d99a0486499",
		name: "Группа кадрового администрирования",
	},
	{
		id: "5f67d8d3-f315-494b-9925-01c186904ef0",
		name: "Группа подбора и адаптации",
	},
	{
		id: "74f04a5b-4a2e-4a6f-b897-a67d634b93f1",
		name: "Направление Технической поддержки",
	},
	{ id: "49c9e990-7e85-47cc-bcee-d7dc275c082a", name: "1С" },
	{
		id: "4b4dde26-dd34-4891-8cae-a8e14db58cef",
		name: "Группа развития и мотивации персонала",
	},
	{
		id: "a8ebf83d-1639-401a-9a91-fbcc5686ef28",
		name: "Pre-sale инженеры",
	},
	{ id: "7c40da68-b32e-4122-9036-c214f11f312a", name: "Служба Качества" },
	{
		id: "18d3fa39-ce07-4c7a-b83d-85adf963aa1a",
		name: "Стрим - Хабаровск",
	},
]

const status = ["Коммерческая", "Внутренняя", "Отпуск", "Больничный"]

const chartData = [
	{
		name: "Направление Mobile",
		employees: 2,
		types: [
			{ type: "1", employees: 1 },
			{ type: "3", employees: 1 },
		],
	},
	{
		name: "Лидеры команды",
		employees: 10,
		types: [
			{ type: "1", employees: 3 },
			{ type: "2", employees: 1 },
			{ type: "3", employees: 6 },
		],
	},
	{
		name: "Направление Web-solutions",
		employees: 3,
		types: [
			{ type: "2", employees: 2 },
			{ type: "3", employees: 1 },
		],
	},
	{
		name: "Направление Web",
		employees: 3,
		types: [
			{ type: "3", employees: 2 },
			{ type: "4", employees: 1 },
		],
	},
	{
		name: "Направление Backend",
		employees: 6,
		types: [
			{ type: "1", employees: 1 },
			{ type: "3", employees: 3 },
			{ type: "4", employees: 2 },
		],
	},
	{
		name: "Направление .Net",
		employees: 5,
		types: [
			{ type: "1", employees: 2 },
			{ type: "2", employees: 1 },
			{ type: "3", employees: 2 },
		],
	},
	{
		name: "Направление Frontend",
		employees: 10,
		types: [
			{ type: "1", employees: 3 },
			{ type: "2", employees: 1 },
			{ type: "3", employees: 5 },
			{ type: "4", employees: 1 },
		],
	},
	{
		name: "Направление Java",
		employees: 8,
		types: [
			{ type: "1", employees: 2 },
			{ type: "2", employees: 1 },
			{ type: "3", employees: 4 },
			{ type: "4", employees: 1 },
		],
	},
	{
		name: "Направление Analytics",
		employees: 4,
		types: [
			{ type: "1", employees: 1 },
			{ type: "2", employees: 1 },
			{ type: "4", employees: 2 },
		],
	},
	{
		name: "HR",
		employees: 3,
		types: [
			{ type: "1", employees: 1 },
			{ type: "3", employees: 2 },
		],
	},
	{
		name: "Группа организации мероприятий",
		employees: 12,
		types: [
			{ type: "1", employees: 2 },
			{ type: "2", employees: 7 },
			{ type: "3", employees: 1 },
			{ type: "4", employees: 2 },
		],
	},
	{
		name: "Стрим - Пенза",
		employees: 3,
		types: [
			{ type: "2", employees: 1 },
			{ type: "3", employees: 2 },
		],
	},
	{
		name: "Направление QA",
		employees: 10,
		types: [
			{ type: "1", employees: 2 },
			{ type: "3", employees: 4 },
			{ type: "4", employees: 4 },
		],
	},
	{
		name: "Руководители Отделов",
		employees: 7,
		types: [
			{ type: "1", employees: 3 },
			{ type: "2", employees: 3 },
			{ type: "3", employees: 1 },
		],
	},
	{
		name: "Направление C++",
		employees: 6,
		types: [
			{ type: "1", employees: 2 },
			{ type: "3", employees: 3 },
			{ type: "4", employees: 1 },
		],
	},
	{
		name: "Направление SDET",
		employees: 7,
		types: [
			{ type: "2", employees: 3 },
			{ type: "3", employees: 4 },
		],
	},
	{
		name: "Направление GoLang",
		employees: 3,
		types: [
			{ type: "1", employees: 1 },
			{ type: "2", employees: 1 },
			{ type: "3", employees: 1 },
		],
	},
	{
		name: "Группа Office Support",
		employees: 6,
		types: [
			{ type: "2", employees: 4 },
			{ type: "3", employees: 1 },
			{ type: "4", employees: 1 },
		],
	},
	{
		name: "Направление бизнес-решений",
		employees: 6,
		types: [
			{ type: "1", employees: 1 },
			{ type: "2", employees: 3 },
			{ type: "3", employees: 2 },
		],
	},
	{
		name: "Направление DevOps",
		employees: 5,
		types: [
			{ type: "1", employees: 2 },
			{ type: "2", employees: 2 },
			{ type: "4", employees: 1 },
		],
	},
	{
		name: "Направление PM",
		employees: 2,
		types: [{ type: "4", employees: 2 }],
	},
	{
		name: "Направление PHP",
		employees: 6,
		types: [
			{ type: "1", employees: 2 },
			{ type: "3", employees: 2 },
			{ type: "4", employees: 2 },
		],
	},
	{
		name: "Направление PR",
		employees: 9,
		types: [
			{ type: "1", employees: 2 },
			{ type: "2", employees: 2 },
			{ type: "3", employees: 3 },
			{ type: "4", employees: 2 },
		],
	},
	{
		name: "Топ менеджеры",
		employees: 10,
		types: [
			{ type: "1", employees: 3 },
			{ type: "2", employees: 2 },
			{ type: "3", employees: 3 },
			{ type: "4", employees: 2 },
		],
	},
	{
		name: "IT Support",
		employees: 11,
		types: [
			{ type: "1", employees: 3 },
			{ type: "2", employees: 1 },
			{ type: "3", employees: 3 },
			{ type: "4", employees: 4 },
		],
	},
	{
		name: "Направление Python",
		employees: 1,
		types: [{ type: "4", employees: 1 }],
	},
	{
		name: "Направление Accounting",
		employees: 4,
		types: [
			{ type: "3", employees: 1 },
			{ type: "4", employees: 3 },
		],
	},
	{
		name: "Отдел продаж",
		employees: 10,
		types: [
			{ type: "1", employees: 3 },
			{ type: "2", employees: 2 },
			{ type: "3", employees: 5 },
		],
	},
	{
		name: "Руководители направлений",
		employees: 6,
		types: [
			{ type: "1", employees: 3 },
			{ type: "2", employees: 2 },
			{ type: "4", employees: 1 },
		],
	},
	{
		name: "Юридическое направление",
		employees: 5,
		types: [
			{ type: "1", employees: 2 },
			{ type: "2", employees: 1 },
			{ type: "4", employees: 2 },
		],
	},
	{
		name: "Стрим - Саратов",
		employees: 5,
		types: [
			{ type: "1", employees: 3 },
			{ type: "3", employees: 1 },
			{ type: "4", employees: 1 },
		],
	},
	{
		name: "Лидеры команд (потенциальные)",
		employees: 7,
		types: [
			{ type: "1", employees: 1 },
			{ type: "2", employees: 4 },
			{ type: "3", employees: 2 },
		],
	},
	{
		name: "Направление сопровождения бизнес-процессов",
		employees: 14,
		types: [
			{ type: "1", employees: 4 },
			{ type: "2", employees: 3 },
			{ type: "3", employees: 3 },
			{ type: "4", employees: 4 },
		],
	},
	{
		name: "Стрим - Нижний Новгород",
		employees: 4,
		types: [
			{ type: "1", employees: 1 },
			{ type: "2", employees: 2 },
			{ type: "3", employees: 1 },
		],
	},
	{
		name: "Направление внутренних проектов и автоматизации процессов",
		employees: 10,
		types: [
			{ type: "1", employees: 3 },
			{ type: "2", employees: 2 },
			{ type: "3", employees: 4 },
			{ type: "4", employees: 1 },
		],
	},
	{
		name: "Направление Design",
		employees: 11,
		types: [
			{ type: "1", employees: 6 },
			{ type: "3", employees: 3 },
			{ type: "4", employees: 2 },
		],
	},
	{
		name: "Финансовое направление",
		employees: 10,
		types: [
			{ type: "1", employees: 3 },
			{ type: "2", employees: 2 },
			{ type: "3", employees: 2 },
			{ type: "4", employees: 3 },
		],
	},
	{
		name: "Группа кадрового администрирования",
		employees: 4,
		types: [
			{ type: "1", employees: 2 },
			{ type: "2", employees: 2 },
		],
	},
	{
		name: "Группа подбора и адаптации",
		employees: 15,
		types: [
			{ type: "1", employees: 1 },
			{ type: "2", employees: 6 },
			{ type: "3", employees: 4 },
			{ type: "4", employees: 4 },
		],
	},
	{
		name: "Направление Технической поддержки",
		employees: 6,
		types: [
			{ type: "2", employees: 2 },
			{ type: "3", employees: 4 },
		],
	},
	{
		name: "1С",
		employees: 5,
		types: [
			{ type: "1", employees: 3 },
			{ type: "4", employees: 2 },
		],
	},
	{
		name: "Группа развития и мотивации персонала",
		employees: 7,
		types: [
			{ type: "1", employees: 1 },
			{ type: "2", employees: 1 },
			{ type: "3", employees: 2 },
			{ type: "4", employees: 3 },
		],
	},
	{
		name: "Pre-sale инженеры",
		employees: 13,
		types: [
			{ type: "1", employees: 3 },
			{ type: "2", employees: 4 },
			{ type: "3", employees: 5 },
			{ type: "4", employees: 1 },
		],
	},
	{
		name: "Служба Качества",
		employees: 9,
		types: [
			{ type: "1", employees: 5 },
			{ type: "2", employees: 1 },
			{ type: "3", employees: 2 },
			{ type: "4", employees: 1 },
		],
	},
	{
		name: "Стрим - Хабаровск",
		employees: 3,
		types: [
			{ type: "1", employees: 2 },
			{ type: "2", employees: 1 },
		],
	},
]

export { deps, status, chartData }
