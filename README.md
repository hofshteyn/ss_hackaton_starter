# SimbirSoft Hackaton Starter

## Stack:
- React JS
- SASS
- Mobx
- Typescript
- Node JS
- Express

## Start

Intall all dependencies via command `npm install`
Run `webpack serve` to run client on `3000` port
Run `npm run node-server` to run server on `5000` port