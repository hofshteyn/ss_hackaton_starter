import csv from "csvtojson"
import path from "path"
import { promises as fs } from "fs"

async function main(): Promise<void> {
	const csvFolder = path.join(__dirname, "../src/infra/data/csv")
	const jsonFolder = path.join(__dirname, "../src/infra/data/converted")

	const files = await fs.readdir(csvFolder)
	for (const file of files) {
		const fileName = file.split(".")[0]
		const json = await csv().fromFile(path.join(csvFolder, file))
		await fs.writeFile(path.join(jsonFolder, `${fileName}.json`), JSON.stringify(json))
	}
}
main()
