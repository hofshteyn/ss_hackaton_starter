# StatUs Backend

### Инициализация
 - Настроить под себя ormconfig.json
 - Настроить .env
 - npm run seed:up (Заполнение БД)
 - npm run start:dev (Запуск проекта)

### Скрипты
 - npm run seed:up (Заполнение БД)
 - npm run seed:down (Откат миграций и заполнения БД)
 - npm run scripts:convert (Конвертация CSV в JSON [подробнее](scripts/convertCsvToJson.ts))

### Структура проекта
 - /src (Исходный код)
 - /.temp (Временные файлы)
 - /scripts (Отдельные скрипты)

### Документация к REST API
> GET /statistics/regions
>
> Request Body: Empty
>
> Response: { status: number, message: string, data: { id: string, name: string, employees: number, avrAge: string }[] }
---

> GET /statistics/regions/:id
>
> Request Body: Empty
>
> Response: { status: number, message: string, data: { id: string, name: string, employees: number }[] }
---

> GET /departments
>
> Request Empty
>
> Response: { status: number, message: string, data: { id: string, name: string }[] }
---

> POST /statistics/departments/tasks
>
> Request Body: [AnalyticsTasksByDepartmentBodyDto](src/app/http/statistics/dtos/AnalyticsTasksByDepartmentBodyDto.ts)
>
> Response: { status: number, message: string, data: { name: string, employees: number, types: { type: string, employees: number }[] }[] }
---

> POST /statistics/departments/interviews
>
> Request Body: [AnalyticsInterviewsPercentageBodyDto](src/app/http/statistics/dtos/AnalyticsInterviewsPercentageBodyDto.ts)
>
> Response: { status: number, message: string, data: { name: string, employees: number, interviews: { result: string, employees: number }[] }[] }
---

> POST /statistics/clients/tasks
>
> Request Body: [AnalyticsClientTasksBodyDto](src/app/http/statistics/dtos/AnalyticsClientTasksBodyDto.ts)
>
> Response: { status: number, message: string, data: { name: string, departments: { tasks: number, name: string }[] }[] }
---

> POST /statistics/clients/employment
>
> Request Body: [AnalyticsClientEmploymentBodyDto](src/app/http/statistics/dtos/AnalyticsClientEmploymentBodyDto.ts)
>
> Response: { status: number, message: string, data: { name: string, employees: { employee: string, daysOnProject: number, completed: boolean }[] }[] }
---

> POST /statistics/clients/searching
>
> Request Body: [AnalyticsClientEmploymentBodyDto](src/app/http/statistics/dtos/AnalyticsClientEmploymentBodyDto.ts)
>
> Response: { status: number, message: string, data: { name: string, tasks: { task: string, search: number }[] }[] }
---

> POST /statistics/tasks/statuses
>
> Request Body: [AnalyticsTaskStatusesByDepartmentBodyDto](src/app/http/statistics/dtos/AnalyticsTaskStatusesByDepartmentBodyDto.ts)
>
> Response: { status: number, message: string, data: { department: string, statuses: { status: string, count: number }[] }[] }
---


