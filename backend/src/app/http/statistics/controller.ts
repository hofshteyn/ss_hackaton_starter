import { getRepository, In } from "typeorm"
import { Region } from "../../../domain/regions/entities/Region"
import { Department } from "../../../domain/department/entities/Department"
import { TaskType } from "../../../domain/tasks/constants/TaskType"
import { intervalToSQL } from "../../../domain/core/TimeInterval"
import { Employee } from "../../../domain/employees/entities/Employee"
import { InterviewResult } from "../../../domain/interviews/constants/InterviewResult"
import { AnalyticsTasksByDepartmentBodyDto } from "./dtos/AnalyticsTasksByDepartmentBodyDto"
import { AnalyticsInterviewsPercentageBodyDto } from "./dtos/AnalyticsInterviewsPercentageBodyDto"
import queryClientJSON from "../../../infra/data/converted/table2.json"
import statusesTableJSON from "../../../infra/data/converted/table1.json"
import { AnalyticsClientTasksBodyDto } from "./dtos/AnalyticsClientTasksBodyDto"
import { AnalyticsClientEmploymentBodyDto } from "./dtos/AnalyticsClientEmploymentBodyDto"
import dayjs from "dayjs";
import { AnalyticsTaskStatusesByDepartmentBodyDto } from "./dtos/AnalyticsTaskStatusesByDepartmentBodyDto";

export class StatisticsController {
	static async getByRegions(): Promise<any> {
		return await getRepository(Region)
			.createQueryBuilder("r")
			.select([
				"r.id as id",
				"r.name as name",
				"count(e.id)::int as employees",
				'round(avg(e.age)::float) as "avrAge"',
			])
			.leftJoin("r.cities", "c")
			.leftJoin("c.employees", "e")
			.leftJoin("e.department", "d")
			.groupBy("r.id")
			.getRawMany()
	}

	static async getByRegion(id: string): Promise<any> {
		const region = await getRepository(Region).findOneOrFail(id)
		return await getRepository(Department)
			.createQueryBuilder("d")
			.select(["d.id as id", "d.name as name", "count(e.id)::int as employees"])
			.leftJoin("d.employees", "e")
			.leftJoin("e.city", "c")
			.leftJoin("c.region", "r")
			.where("r.id = :id", { id: region.id })
			.groupBy("d.id")
			.getRawMany()
	}

	static async getTasksByDepartment(options: AnalyticsTasksByDepartmentBodyDto): Promise<any> {
		const departments: Department[] = options.departments
			? await getRepository(Department).find({ name: In([...options.departments]) })
			: await getRepository(Department).find()

		const result = []
		for (const department of departments) {
			const query = await getRepository(Employee).createQueryBuilder("e")
			query.select(["t.type as type", "count(e.id)::int as employees"])
			query.leftJoin("e.department", "d")
			query.leftJoin("e.tasks", "t")
			query.where("d.id = :id", { id: department.id })
			query.andWhere("t.type is not null")
			if (options.types && options.types.length) query.where("t.type IN (:...types)", { types: options.types })
			if (options.interval) query.andWhere(`t."startDate" > ${intervalToSQL(options.interval)}`)
			query.groupBy("t.type")
			query.addGroupBy("d.name")
			const data = await query.getRawMany<{ type: TaskType; employees: number }>()

			const employees = data.reduce((acc, item) => (acc += item.employees), 0)
			result.push({ name: department.name, employees, types: data })
		}

		return result
	}

	static async getInterviewPercentage(options: AnalyticsInterviewsPercentageBodyDto): Promise<any> {
		const departments: Department[] = options.departments
			? await getRepository(Department).find({ name: In([...options.departments]) })
			: await getRepository(Department).find()

		const result = []

		for (const department of departments) {
			const query = await getRepository(Employee).createQueryBuilder("e")
			query.select(["i.result as result", "count(e.id)::int as employees"])
			query.leftJoin("e.department", "d")
			query.leftJoin("e.interviews", "i")
			query.where("d.id = :id", { id: department.id })
			query.andWhere("i.result is not null")
			if (options.interval) query.andWhere(`i.date > ${intervalToSQL(options.interval)}`)
			query.groupBy("i.result")
			query.addGroupBy("d.name")
			const data = await query.getRawMany<{ result: InterviewResult; employees: number }>()

			const employees = data.reduce((acc, item) => (acc += item.employees), 0)
			result.push({ name: department.name, employees, interviews: data })
		}

		return result
	}

	static async getClientTasksByDepartments(options: AnalyticsClientTasksBodyDto): Promise<any> {
		let clients: string[] = options.clients || []
		if (clients.length === 0) {
			const uniqueClients: Set<string> = new Set<string>()
			queryClientJSON.forEach((t) => uniqueClients.add(t["Клиент"]))
			clients = Array.from(uniqueClients)
		}

		const result = []
		for (const client of clients) {
			const taskCounter: Map<string, number> = new Map<string, number>()
			for (const task of queryClientJSON) {
				if (task["Клиент"] === client) {
					if (!taskCounter.has(task["Отдел"])) {
						taskCounter.set(task["Отдел"], 1)
					} else {
						taskCounter.set(task["Отдел"], taskCounter.get(task["Отдел"])! + 1)
					}
				}
			}

			const subResult = []
			for (const [key, value] of taskCounter) {
				subResult.push({ name: key, tasks: value })
			}

			result.push({ name: client, departments: subResult })
		}

		return result
	}

	static async getEmployeesDaysOnClientsProjects(options: AnalyticsClientEmploymentBodyDto): Promise<any> {
		let clients: string[] = options.clients || []
		if (clients.length === 0) {
			const uniqueClients: Set<string> = new Set<string>()
			queryClientJSON.forEach((t) => uniqueClients.add(t["Клиент"]))
			clients = Array.from(uniqueClients)
		}

		const result: any = []
		for (const client of clients) {
			const employeeTime: Map<string, [number, boolean]> = new Map<string, [number, boolean]>()
			for (const row of queryClientJSON) {
				if (row["Клиент"] === client && row["Пользовательское поле (Фактическая дата подключения)"]) {
					const employee = row["Запрос на специалиста"]

					const completed = !!row["Пользовательское поле (Фактическая дата отключения)"]
					const started = dayjs(row["Пользовательское поле (Фактическая дата подключения)"])
					const ended = completed ? dayjs(row["Пользовательское поле (Фактическая дата отключения)"]) : dayjs()

					const duration = started.isBefore(ended) ? ended.diff(started) : 0
					const days = duration === 0 ? 0 : Math.round(duration / (1000 * 60 * 60 * 24))

					employeeTime.set(employee, [days, completed])
				}
			}

			const subResult = []
			for (const [key, value] of employeeTime) {
				subResult.push({ employee: key, daysOnProject: value[0], completed: value[1] })
			}

			result.push({ name: client, employees: subResult })
		}

		return result
	}

	static async getClientSearchingDays(options: { clients?: string[] }): Promise<any> {
		let clients: string[] = options.clients || []
		if (clients.length === 0) {
			const uniqueClients: Set<string> = new Set<string>()
			queryClientJSON.forEach((t) => uniqueClients.add(t["Клиент"]))
			clients = Array.from(uniqueClients)
		}

		const result: any = []
		for (const client of clients) {
			const employeeTime: Map<string, number> = new Map<string, number>()
			for (const row of queryClientJSON) {
				if (row["Клиент"] === client) {
					const employee = row["Запрос на специалиста"]

					const createdDate = dayjs(row["Cоздано"])
					const connected = row["Пользовательское поле (Фактическая дата подключения)"]
						? dayjs(row["Пользовательское поле (Фактическая дата подключения)"]) : dayjs()

					const duration = connected.isBefore(createdDate)
						? 0 : connected.diff(createdDate)
					const days = duration === 0 ? 0 : Math.round(duration / (1000 * 60 * 60 * 24))

					employeeTime.set(employee, days)
				}
			}

			const subResult = []
			for (const [key, value] of employeeTime) {
				subResult.push({ task: key, search: value })
			}

			result.push({ name: client, tasks: subResult })
		}

		return result
	}

	static async getLastTasks(options: AnalyticsTaskStatusesByDepartmentBodyDto): Promise<any> {
		const table: any[] = statusesTableJSON as any[]

		let departments: string[] = options.departments || []
		if (departments.length === 0) {
			const uniqueDepartments: Set<string> = new Set<string>()
			table.forEach((t) => uniqueDepartments.add(t["Направление"]))
			departments = Array.from(uniqueDepartments)
		}

		const result = []

		const taskStatus: Map<string, [string, string]> = new Map<string, [string, string]>()
		for (const row of table) {
			if (departments.includes(row["Направление"])) {
				taskStatus.set(row["Исходный запрос"], [row["Статус в котором находился запрос"], row["Направление"]])
			}

		}

		for (const [key, value] of taskStatus) {
			result.push({ task: key, status: value[0], department: value[1] })
		}

		const aggregator: { department: string, statuses: { status: string, count: number }[] }[] = []
		for (const item of result) {
			const added = aggregator.find((i) => i.department === item.department)
			if (!added) {
				aggregator.push({ department: item.department, statuses: [{ status: item.status, count: 1 }] })
			} else {
				const statusAdded = added.statuses.find((s) => s.status === item.status)
				if (!statusAdded) {
					added.statuses.push({ status: item.status, count: 1 })
				} else {
					statusAdded.count += 1
				}
			}
		}

		return aggregator
	}
}
