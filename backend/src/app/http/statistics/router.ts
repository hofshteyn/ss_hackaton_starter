import express, { Request, Response } from "express"
import { HttpResponse } from "../HttpResponse"
import { RequestPart, Validator } from "../shared/guards/Validator"
import { HttpStatus } from "../shared/HttpStatus"
import { StatisticsController } from "./controller"
import { AnalyticsTasksByDepartmentBodyDto } from "./dtos/AnalyticsTasksByDepartmentBodyDto"
import { AnalyticsInterviewsPercentageBodyDto } from "./dtos/AnalyticsInterviewsPercentageBodyDto"
import { AnalyticsClientTasksBodyDto } from "./dtos/AnalyticsClientTasksBodyDto"
import { AnalyticsClientEmploymentBodyDto } from "./dtos/AnalyticsClientEmploymentBodyDto"
import { AnalyticsTaskStatusesByDepartmentBodyDto } from "./dtos/AnalyticsTaskStatusesByDepartmentBodyDto";

const router: express.Router = express.Router()

router.get("/regions", async (req: Request, res: Response) => {
	const data = await StatisticsController.getByRegions()
	return HttpResponse.ok(res, HttpStatus.Ok, data)
})

router.get("/regions/:id", async (req: Request, res: Response) => {
	const data = await StatisticsController.getByRegion(req.params.id)
	return HttpResponse.ok(res, HttpStatus.Ok, data)
})

router.post(
	"/departments/tasks",
	[Validator(AnalyticsTasksByDepartmentBodyDto, RequestPart.Body)],
	async (req: Request, res: Response) => {
		const data = await StatisticsController.getTasksByDepartment(req.body)
		return HttpResponse.ok(res, HttpStatus.Ok, data)
	}
)

router.post(
	"/departments/interviews",
	[Validator(AnalyticsInterviewsPercentageBodyDto, RequestPart.Body)],
	async (req: Request, res: Response) => {
		const data = await StatisticsController.getInterviewPercentage(req.body)
		return HttpResponse.ok(res, HttpStatus.Ok, data)
	}
)

router.post("/clients/tasks", [Validator(AnalyticsClientTasksBodyDto, RequestPart.Body)], async (req: Request, res: Response) => {
	const data = await StatisticsController.getClientTasksByDepartments(req.body)
	return HttpResponse.ok(res, HttpStatus.Ok, data)
})

router.post("/clients/employment", [Validator(AnalyticsClientEmploymentBodyDto, RequestPart.Body)], async (req: Request, res: Response) => {
	const data = await StatisticsController.getEmployeesDaysOnClientsProjects(req.body)
	return HttpResponse.ok(res, HttpStatus.Ok, data)
})

router.post("/clients/searching", [Validator(AnalyticsClientEmploymentBodyDto, RequestPart.Body)], async (req: Request, res: Response) => {
	const data = await StatisticsController.getClientSearchingDays(req.body)
	return HttpResponse.ok(res, HttpStatus.Ok, data)
})

router.get("/tasks/statuses", [Validator(AnalyticsTaskStatusesByDepartmentBodyDto, RequestPart.Body)], async (req: Request, res: Response) => {
	const data = await StatisticsController.getLastTasks(req.body)
	return HttpResponse.ok(res, HttpStatus.Ok, data)
})

export { router as StatisticsRouter }
