import { IsString } from "class-validator";

export class AnalyticsTaskStatusesByDepartmentBodyDto {
	@IsString({ each: true })
	departments!: string[]
}
