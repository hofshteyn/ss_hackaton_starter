import { IsString } from "class-validator"

export class AnalyticsClientTasksBodyDto {
	@IsString({ each: true })
	clients!: string[]
}
