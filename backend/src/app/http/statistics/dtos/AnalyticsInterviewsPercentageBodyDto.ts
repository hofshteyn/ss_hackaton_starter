import { IsEnum, IsString } from "class-validator"
import { TimeInterval } from "../../../../domain/core/TimeInterval"

export class AnalyticsInterviewsPercentageBodyDto {
	@IsString({ each: true })
	departments!: string[]

	@IsEnum(TimeInterval)
	interval!: TimeInterval
}
