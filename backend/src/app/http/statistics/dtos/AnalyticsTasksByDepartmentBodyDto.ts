import { IsEnum, IsNumber, IsString } from "class-validator"
import { TaskType } from "../../../../domain/tasks/constants/TaskType"
import { TimeInterval } from "../../../../domain/core/TimeInterval"

export class AnalyticsTasksByDepartmentBodyDto {
	@IsString({ each: true })
	departments!: string[]

	@IsNumber({}, { each: true })
	types!: TaskType[]

	@IsEnum(TimeInterval)
	interval!: TimeInterval
}
