import { IsString } from "class-validator";

export class AnalyticsClientEmploymentBodyDto {
	@IsString({ each: true })
	clients!: string[]
}
