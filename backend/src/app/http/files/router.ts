import express, { Request, Response } from "express"
import { FileType, Upload } from "../shared/middlewares/Upload"
import { HttpResponse } from "../HttpResponse"
import { HttpStatus } from "../shared/HttpStatus"
import { FilesController } from "./controller"
import { HttpError } from "../shared/HttpError"

const router: express.Router = express.Router()

router.post("/convert", [Upload([FileType.Csv]).single("file")], async (req: Request, res: Response) => {
	if (!req.file) {
		throw new HttpError("invalid file", HttpStatus.BadRequest)
	}
	const data = await FilesController.convert(req.file.path)
	return HttpResponse.ok(res, HttpStatus.Ok, data)
})

export { router as FilesRouter }
