import csv from "csvtojson"
import { promises } from "fs"

export class FilesController {
	static async convert(filePath: string): Promise<any> {
		const data = await csv().fromFile(filePath)
		await promises.rm(filePath)
		return data
	}
}
