import express, { Request, Response } from "express"
import { HttpResponse } from "../HttpResponse"
import { HttpStatus } from "../shared/HttpStatus"
import { getRepository } from "typeorm"
import { Region } from "../../../domain/regions/entities/Region"

const router: express.Router = express.Router()

router.get("/", async (req: Request, res: Response) => {
	const regions = await getRepository(Region).find()
	return HttpResponse.ok(res, HttpStatus.Ok, regions)
})

export { router as RegionsRouter }
