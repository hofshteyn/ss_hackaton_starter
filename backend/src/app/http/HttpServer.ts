import express, { Application } from "express"
import "express-async-errors"
import helmet from "helmet"
import cors from "cors"
import morgan from "morgan"
import "../../infra/morgan"
import { Logger } from "../../infra/logger"
import { Config } from "../../infra/config"
import { RegionsRouter } from "./regions/router"
import { StatisticsRouter } from "./statistics/router"
import { DepartmentsRouter } from "./departments/router"
import { ErrorHandler } from "./shared/middlewares/ErrorHandler"
import { FilesRouter } from "./files/router"

const prefix = "/api/v1"

export class HttpServer {
	private static app: Application

	static up(): Promise<void> {
		HttpServer.app = express()
		HttpServer.app.use(morgan(":customTime [Http] :method :url => :status (:response-time ms)"))
		HttpServer.app.use(express.json({ limit: "50mb" }))
		HttpServer.app.use(cors())
		HttpServer.app.use(helmet())

		HttpServer.app.use(`${prefix}/regions`, RegionsRouter)
		HttpServer.app.use(`${prefix}/departments`, DepartmentsRouter)
		HttpServer.app.use(`${prefix}/statistics`, StatisticsRouter)
		HttpServer.app.use(`${prefix}/files`, FilesRouter)

		HttpServer.app.use(ErrorHandler)

		return new Promise((res) => {
			HttpServer.app.listen(parseInt(Config.PORT), Config.HOST, () => {
				Logger.success("HttpServer", `Up on http://${Config.HOST}:${Config.PORT}`)
				res()
			})
		})
	}
}
