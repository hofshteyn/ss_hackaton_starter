import path from "path"
import multer from "multer"
import { Request } from "express"
import { Coder } from "../../../../infra/modules/coder"

export enum FileType {
	Csv = 1,
}

export const Upload = (types?: FileType[]) => {
	return multer({
		storage: multer.diskStorage({
			destination: (req, file, cb) => {
				cb(null, path.join(__dirname, "../../../../../.temp"))
			},
			filename: (req, file, cb) => {
				cb(null, [Date.now(), Coder.generate(8), file.originalname].join("-"))
			},
		}),
		fileFilter(req: Request, file: Express.Multer.File, callback: multer.FileFilterCallback) {
			if (!types) {
				return callback(null, false)
			}
			if (types.includes(FileType.Csv) && file.mimetype !== "text/csv") {
				return callback(null, false)
			}
			return callback(null, true)
		},
	})
}
