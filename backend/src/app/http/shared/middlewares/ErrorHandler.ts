import { Request, Response, NextFunction } from "express"
import { Config } from "../../../../infra/config"
import { HttpError } from "../HttpError"

export const ErrorHandler = (err: Error, req: Request, res: Response, _: NextFunction) => {
	if (err instanceof HttpError) {
		err.statusCode = err.statusCode || 500
		return res.status(err.statusCode).send({ status: "ERROR", statusCode: err.statusCode, details: err.message })
	}

	if (Config.NODE_ENV === "development") {
		return res.status(500).send({ status: "ERROR", statusCode: 500, message: err.message })
	}
	return res.status(500).send({ status: "ERROR", statusCode: 500, message: "Internal error" })
}
