export class HttpError extends Error {
	statusCode: number
	details: string

	constructor(details: string, statusCode?: number) {
		super(details)
		this.statusCode = statusCode || 500
		this.details = details || ""
	}
}
