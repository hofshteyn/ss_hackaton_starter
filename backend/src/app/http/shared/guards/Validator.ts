import { Request, Response, NextFunction } from "express"
import { plainToClass } from "class-transformer"
import { validate, ValidationError } from "class-validator"
import { HttpError } from "../HttpError"

export enum RequestPart {
	Body = 1,
	Query,
}

export const Validator = (dtoClass: any, part: RequestPart) => {
	return async (req: Request, res: Response, next: NextFunction): Promise<void> => {
		let output: any

		if (part === RequestPart.Body) {
			output = plainToClass(dtoClass, req.body)
		}
		if (part === RequestPart.Query) {
			output = plainToClass(dtoClass, req.query)
		}

		const errors = await validate(output, { skipMissingProperties: true })
		if (errors.length > 0) {
			throw new HttpError(serializeErrors(errors), 400)
		}

		return next()
	}
}

const serializeErrors = (errors: ValidationError[]) => {
	let errorMessage = "Bad Request: "
	for (const error of errors) {
		for (const constrain in error.constraints) {
			errorMessage += error.constraints[constrain] + "; "
		}
	}
	return errorMessage.slice(0, -2)
}
