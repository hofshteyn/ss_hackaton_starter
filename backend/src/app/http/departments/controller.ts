import { Department } from "../../../domain/department/entities/Department"
import { getRepository } from "typeorm"

export class DepartmentsController {
	static get(): Promise<Department[]> {
		return getRepository(Department).find()
	}
}
