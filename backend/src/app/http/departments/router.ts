import express, { Request, Response } from "express"
import { HttpResponse } from "../HttpResponse"
import { HttpStatus } from "../shared/HttpStatus"
import { DepartmentsController } from "./controller"

const router: express.Router = express.Router()

router.get("/", async (req: Request, res: Response) => {
	const departments = await DepartmentsController.get()
	return HttpResponse.ok(res, HttpStatus.Ok, departments)
})

export { router as DepartmentsRouter }
