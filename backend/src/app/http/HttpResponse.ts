import { Response } from "express"
import { HttpStatus } from "./shared/HttpStatus"

export class HttpResponse {
	static ok<T>(res: Response, status: number, data?: T, options?: { pagination: boolean }): void {
		return res
			.status(status)
			.send({
				status,
				message: HttpStatus[status],
				data,
				meta: options?.pagination ? { count: 0, limit: 0, offset: 0 } : undefined,
			})
			.end()
	}
}
