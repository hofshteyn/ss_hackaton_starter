import Keyv from "keyv"
import { Config } from "../config"
import { Logger } from "../logger"

export class Redis {
	static connection: Keyv

	static async connect(): Promise<void> {
		this.connection = new Keyv<any>(Config.REDIS_URL)
		Logger.success("Redis", "Connected")
	}

	static async set<T>(key: string, value: T, expires?: number): Promise<void> {
		await this.connection.set(key, value, expires)
	}

	static get<T>(key: string): Promise<T> {
		return this.connection.get(key)
	}

	static async delete(key: string): Promise<void> {
		await this.connection.delete(key)
	}
}
