import { Random } from "../random"

export class Coder {
	static dict: string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"

	static generate(len: number): string {
		let code = ""
		for (let i = 0; i < len; i++) {
			code += Coder.dict[Random.fromIntervalInt(0, Coder.dict.length)]
		}
		return code
	}
}
