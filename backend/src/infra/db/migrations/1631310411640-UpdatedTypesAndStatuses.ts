import { MigrationInterface, QueryRunner } from "typeorm"

export class UpdatedTypesAndStatuses1631310411640 implements MigrationInterface {
	name = "UpdatedTypesAndStatuses1631310411640"

	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`CREATE TYPE "public"."employee_sex_enum" AS ENUM('1', '2')`)
		await queryRunner.query(`ALTER TABLE "public"."employee" ADD "sex" "public"."employee_sex_enum" NOT NULL`)
		await queryRunner.query(`ALTER TYPE "public"."task_status_enum" RENAME TO "task_status_enum_old"`)
		await queryRunner.query(`CREATE TYPE "public"."task_status_enum" AS ENUM('1', '2', '3', '4', '5')`)
		await queryRunner.query(`ALTER TABLE "public"."task" ALTER COLUMN "status" DROP DEFAULT`)
		await queryRunner.query(
			`ALTER TABLE "public"."task" ALTER COLUMN "status" TYPE "public"."task_status_enum" USING "status"::"text"::"public"."task_status_enum"`
		)
		await queryRunner.query(`ALTER TABLE "public"."task" ALTER COLUMN "status" SET DEFAULT '1'`)
		await queryRunner.query(`DROP TYPE "public"."task_status_enum_old"`)
		await queryRunner.query(`ALTER TYPE "public"."customer_type_enum" RENAME TO "customer_type_enum_old"`)
		await queryRunner.query(`CREATE TYPE "public"."customer_type_enum" AS ENUM('1', '2', '3')`)
		await queryRunner.query(
			`ALTER TABLE "public"."customer" ALTER COLUMN "type" TYPE "public"."customer_type_enum" USING "type"::"text"::"public"."customer_type_enum"`
		)
		await queryRunner.query(`DROP TYPE "public"."customer_type_enum_old"`)
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`CREATE TYPE "public"."customer_type_enum_old" AS ENUM('1', '2')`)
		await queryRunner.query(
			`ALTER TABLE "public"."customer" ALTER COLUMN "type" TYPE "public"."customer_type_enum_old" USING "type"::"text"::"public"."customer_type_enum_old"`
		)
		await queryRunner.query(`DROP TYPE "public"."customer_type_enum"`)
		await queryRunner.query(`ALTER TYPE "public"."customer_type_enum_old" RENAME TO "customer_type_enum"`)
		await queryRunner.query(`CREATE TYPE "public"."task_status_enum_old" AS ENUM('1', '2')`)
		await queryRunner.query(`ALTER TABLE "public"."task" ALTER COLUMN "status" DROP DEFAULT`)
		await queryRunner.query(
			`ALTER TABLE "public"."task" ALTER COLUMN "status" TYPE "public"."task_status_enum_old" USING "status"::"text"::"public"."task_status_enum_old"`
		)
		await queryRunner.query(`ALTER TABLE "public"."task" ALTER COLUMN "status" SET DEFAULT '1'`)
		await queryRunner.query(`DROP TYPE "public"."task_status_enum"`)
		await queryRunner.query(`ALTER TYPE "public"."task_status_enum_old" RENAME TO "task_status_enum"`)
		await queryRunner.query(`ALTER TABLE "public"."employee" DROP COLUMN "sex"`)
		await queryRunner.query(`DROP TYPE "public"."employee_sex_enum"`)
	}
}
