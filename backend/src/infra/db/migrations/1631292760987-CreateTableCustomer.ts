import { MigrationInterface, QueryRunner } from "typeorm"

export class CreateTableCustomer1631292760987 implements MigrationInterface {
	name = "CreateTableCustomer1631292760987"

	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`CREATE TYPE "customer_type_enum" AS ENUM('1', '2')`)
		await queryRunner.query(
			`CREATE TABLE "customer" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying NOT NULL, "type" "customer_type_enum" NOT NULL, "description" text NOT NULL, CONSTRAINT "PK_a7a13f4cacb744524e44dfdad32" PRIMARY KEY ("id"))`
		)
		await queryRunner.query(`ALTER TABLE "public"."task" ADD "customerId" uuid`)
		await queryRunner.query(
			`ALTER TABLE "public"."task" ADD CONSTRAINT "FK_2997f01b00a9dc7d7fec2db89ae" FOREIGN KEY ("customerId") REFERENCES "customer"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
		)
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`ALTER TABLE "public"."task" DROP CONSTRAINT "FK_2997f01b00a9dc7d7fec2db89ae"`)
		await queryRunner.query(`ALTER TABLE "public"."task" DROP COLUMN "customerId"`)
		await queryRunner.query(`DROP TABLE "customer"`)
		await queryRunner.query(`DROP TYPE "customer_type_enum"`)
	}
}
