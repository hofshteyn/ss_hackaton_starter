import { MigrationInterface, QueryRunner } from "typeorm"

export class CreateTableInterview1631307902825 implements MigrationInterface {
	name = "CreateTableInterview1631307902825"

	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`CREATE TYPE "interview_status_enum" AS ENUM('1', '2', '3', '4', '5')`)
		await queryRunner.query(`CREATE TYPE "interview_result_enum" AS ENUM('1', '2', '3', '4')`)
		await queryRunner.query(
			`CREATE TABLE "interview" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "status" "interview_status_enum" NOT NULL, "result" "interview_result_enum" NOT NULL, "customerId" uuid, "employeeId" uuid, CONSTRAINT "PK_44c49a4feadefa5c6fa78bfb7d1" PRIMARY KEY ("id"))`
		)
		await queryRunner.query(`ALTER TYPE "public"."task_type_enum" RENAME TO "task_type_enum_old"`)
		await queryRunner.query(`CREATE TYPE "public"."task_type_enum" AS ENUM('1', '2', '3', '4')`)
		await queryRunner.query(
			`ALTER TABLE "public"."task" ALTER COLUMN "type" TYPE "public"."task_type_enum" USING "type"::"text"::"public"."task_type_enum"`
		)
		await queryRunner.query(`DROP TYPE "public"."task_type_enum_old"`)
		await queryRunner.query(
			`ALTER TABLE "interview" ADD CONSTRAINT "FK_9c5bde679f4c5cef58a3f869182" FOREIGN KEY ("customerId") REFERENCES "customer"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
		)
		await queryRunner.query(
			`ALTER TABLE "interview" ADD CONSTRAINT "FK_3f20acd5b59aee1ae49d6c7d724" FOREIGN KEY ("employeeId") REFERENCES "employee"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
		)
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`ALTER TABLE "interview" DROP CONSTRAINT "FK_3f20acd5b59aee1ae49d6c7d724"`)
		await queryRunner.query(`ALTER TABLE "interview" DROP CONSTRAINT "FK_9c5bde679f4c5cef58a3f869182"`)
		await queryRunner.query(`CREATE TYPE "public"."task_type_enum_old" AS ENUM('1', '2', '3')`)
		await queryRunner.query(
			`ALTER TABLE "public"."task" ALTER COLUMN "type" TYPE "public"."task_type_enum_old" USING "type"::"text"::"public"."task_type_enum_old"`
		)
		await queryRunner.query(`DROP TYPE "public"."task_type_enum"`)
		await queryRunner.query(`ALTER TYPE "public"."task_type_enum_old" RENAME TO "task_type_enum"`)
		await queryRunner.query(`DROP TABLE "interview"`)
		await queryRunner.query(`DROP TYPE "interview_result_enum"`)
		await queryRunner.query(`DROP TYPE "interview_status_enum"`)
	}
}
