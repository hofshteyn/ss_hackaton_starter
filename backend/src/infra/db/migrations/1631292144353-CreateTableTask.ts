import { MigrationInterface, QueryRunner } from "typeorm"

export class CreateTableTask1631292144353 implements MigrationInterface {
	name = "CreateTableTask1631292144353"

	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`CREATE TYPE "task_status_enum" AS ENUM('1', '2')`)
		await queryRunner.query(`CREATE TYPE "task_type_enum" AS ENUM('1', '2', '3')`)
		await queryRunner.query(
			`CREATE TABLE "task" ("number" character varying NOT NULL, "status" "task_status_enum" NOT NULL DEFAULT '1', "type" "task_type_enum" NOT NULL, "startDate" TIMESTAMP NOT NULL, "endDate" TIMESTAMP NOT NULL, "epic" character varying NOT NULL, CONSTRAINT "UQ_929a4ba247d50034d52549f2b76" UNIQUE ("epic"), CONSTRAINT "PK_93169d9cdf2f722b2d47a4d893b" PRIMARY KEY ("number"))`
		)
		await queryRunner.query(
			`CREATE TABLE "task_employee" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "time" double precision NOT NULL, "taskNumber" character varying, "employeeId" uuid, CONSTRAINT "PK_a198e46a9402f905c8c2fc37c25" PRIMARY KEY ("id"))`
		)
		await queryRunner.query(
			`ALTER TABLE "task_employee" ADD CONSTRAINT "FK_86351763ed24feeea2895eb2cfc" FOREIGN KEY ("taskNumber") REFERENCES "task"("number") ON DELETE NO ACTION ON UPDATE NO ACTION`
		)
		await queryRunner.query(
			`ALTER TABLE "task_employee" ADD CONSTRAINT "FK_f38b1bd46f8831704348003bbff" FOREIGN KEY ("employeeId") REFERENCES "employee"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
		)
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`ALTER TABLE "task_employee" DROP CONSTRAINT "FK_f38b1bd46f8831704348003bbff"`)
		await queryRunner.query(`ALTER TABLE "task_employee" DROP CONSTRAINT "FK_86351763ed24feeea2895eb2cfc"`)
		await queryRunner.query(`DROP TABLE "task_employee"`)
		await queryRunner.query(`DROP TABLE "task"`)
		await queryRunner.query(`DROP TYPE "task_type_enum"`)
		await queryRunner.query(`DROP TYPE "task_status_enum"`)
	}
}
