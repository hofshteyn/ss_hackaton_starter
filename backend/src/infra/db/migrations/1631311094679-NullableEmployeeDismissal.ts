import { MigrationInterface, QueryRunner } from "typeorm"

export class NullableEmployeeDismissal1631311094679 implements MigrationInterface {
	name = "NullableEmployeeDismissal1631311094679"

	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`ALTER TABLE "public"."employee" ALTER COLUMN "dismissal" DROP NOT NULL`)
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`ALTER TABLE "public"."employee" ALTER COLUMN "dismissal" SET NOT NULL`)
	}
}
