import { MigrationInterface, QueryRunner } from "typeorm"

export class CreateTableEmployee1631291344097 implements MigrationInterface {
	name = "CreateTableEmployee1631291344097"

	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(
			`CREATE TABLE "employee" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying NOT NULL, "login" character varying NOT NULL, "employment" TIMESTAMP NOT NULL, "dismissal" TIMESTAMP NOT NULL, "cityId" uuid, CONSTRAINT "PK_3c2bc72f03fd5abbbc5ac169498" PRIMARY KEY ("id"))`
		)
		await queryRunner.query(
			`ALTER TABLE "employee" ADD CONSTRAINT "FK_eef86b711e6048de0951f8b3f43" FOREIGN KEY ("cityId") REFERENCES "city"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
		)
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`ALTER TABLE "employee" DROP CONSTRAINT "FK_eef86b711e6048de0951f8b3f43"`)
		await queryRunner.query(`DROP TABLE "employee"`)
	}
}
