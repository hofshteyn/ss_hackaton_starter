import { MigrationInterface, QueryRunner } from "typeorm"

export class CreateTableEpic1631308620009 implements MigrationInterface {
	name = "CreateTableEpic1631308620009"

	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(
			`CREATE TABLE "epic" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying NOT NULL, "customerId" uuid, CONSTRAINT "PK_1c02d43d72c5150bc888d452397" PRIMARY KEY ("id"))`
		)
		await queryRunner.query(`ALTER TABLE "public"."interview" ADD "epicId" uuid`)
		await queryRunner.query(
			`ALTER TABLE "epic" ADD CONSTRAINT "FK_4c1a0a23d96d50c9e79aee3b98f" FOREIGN KEY ("customerId") REFERENCES "customer"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
		)
		await queryRunner.query(
			`ALTER TABLE "public"."interview" ADD CONSTRAINT "FK_839e2c7f299cff02313081c6755" FOREIGN KEY ("epicId") REFERENCES "epic"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
		)
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`ALTER TABLE "public"."interview" DROP CONSTRAINT "FK_839e2c7f299cff02313081c6755"`)
		await queryRunner.query(`ALTER TABLE "epic" DROP CONSTRAINT "FK_4c1a0a23d96d50c9e79aee3b98f"`)
		await queryRunner.query(`ALTER TABLE "public"."interview" DROP COLUMN "epicId"`)
		await queryRunner.query(`DROP TABLE "epic"`)
	}
}
