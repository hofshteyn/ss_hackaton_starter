import { MigrationInterface, QueryRunner } from "typeorm"

export class RelationEmployeeTask1631341320352 implements MigrationInterface {
	name = "RelationEmployeeTask1631341320352"

	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(
			`CREATE TABLE "employee_tasks_task" ("employeeId" uuid NOT NULL, "taskNumber" character varying NOT NULL, CONSTRAINT "PK_1424066b19ed3961dc4b95d95bf" PRIMARY KEY ("employeeId", "taskNumber"))`
		)
		await queryRunner.query(
			`CREATE INDEX "IDX_eae5eea1c6a3fcf4a2c95f1a5f" ON "employee_tasks_task" ("employeeId") `
		)
		await queryRunner.query(
			`CREATE INDEX "IDX_25ff1c5e99558cac64cb49cc42" ON "employee_tasks_task" ("taskNumber") `
		)
		await queryRunner.query(
			`ALTER TABLE "employee_tasks_task" ADD CONSTRAINT "FK_eae5eea1c6a3fcf4a2c95f1a5fe" FOREIGN KEY ("employeeId") REFERENCES "employee"("id") ON DELETE CASCADE ON UPDATE CASCADE`
		)
		await queryRunner.query(
			`ALTER TABLE "employee_tasks_task" ADD CONSTRAINT "FK_25ff1c5e99558cac64cb49cc42b" FOREIGN KEY ("taskNumber") REFERENCES "task"("number") ON DELETE NO ACTION ON UPDATE NO ACTION`
		)
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`ALTER TABLE "employee_tasks_task" DROP CONSTRAINT "FK_25ff1c5e99558cac64cb49cc42b"`)
		await queryRunner.query(`ALTER TABLE "employee_tasks_task" DROP CONSTRAINT "FK_eae5eea1c6a3fcf4a2c95f1a5fe"`)
		await queryRunner.query(`DROP INDEX "IDX_25ff1c5e99558cac64cb49cc42"`)
		await queryRunner.query(`DROP INDEX "IDX_eae5eea1c6a3fcf4a2c95f1a5f"`)
		await queryRunner.query(`DROP TABLE "employee_tasks_task"`)
	}
}
