import { MigrationInterface, QueryRunner } from "typeorm"

export class RemovedRelationBetweenCustomerAndInterview1631308837321 implements MigrationInterface {
	name = "RemovedRelationBetweenCustomerAndInterview1631308837321"

	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`ALTER TABLE "public"."interview" DROP CONSTRAINT "FK_9c5bde679f4c5cef58a3f869182"`)
		await queryRunner.query(`ALTER TABLE "public"."interview" DROP COLUMN "customerId"`)
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`ALTER TABLE "public"."interview" ADD "customerId" uuid`)
		await queryRunner.query(
			`ALTER TABLE "public"."interview" ADD CONSTRAINT "FK_9c5bde679f4c5cef58a3f869182" FOREIGN KEY ("customerId") REFERENCES "customer"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
		)
	}
}
