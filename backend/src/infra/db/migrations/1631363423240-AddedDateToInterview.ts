import { MigrationInterface, QueryRunner } from "typeorm"

export class AddedDateToInterview1631363423240 implements MigrationInterface {
	name = "AddedDateToInterview1631363423240"

	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`ALTER TABLE "public"."interview" ADD "date" TIMESTAMP NOT NULL`)
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`ALTER TABLE "public"."interview" DROP COLUMN "date"`)
	}
}
