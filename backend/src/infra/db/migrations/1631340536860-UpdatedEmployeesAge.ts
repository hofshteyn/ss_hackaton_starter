import { MigrationInterface, QueryRunner } from "typeorm"

export class UpdatedEmployeesAge1631340536860 implements MigrationInterface {
	name = "UpdatedEmployeesAge1631340536860"

	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`ALTER TABLE "public"."employee" ADD "age" integer NOT NULL`)
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`ALTER TABLE "public"."employee" DROP COLUMN "age"`)
	}
}
