import { MigrationInterface, QueryRunner } from "typeorm"

export class RemovedRelationBetweenTaskAndCustomer1631308916932 implements MigrationInterface {
	name = "RemovedRelationBetweenTaskAndCustomer1631308916932"

	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`ALTER TABLE "public"."task" DROP CONSTRAINT "FK_2997f01b00a9dc7d7fec2db89ae"`)
		await queryRunner.query(`ALTER TABLE "public"."task" DROP COLUMN "customerId"`)
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`ALTER TABLE "public"."task" ADD "customerId" uuid`)
		await queryRunner.query(
			`ALTER TABLE "public"."task" ADD CONSTRAINT "FK_2997f01b00a9dc7d7fec2db89ae" FOREIGN KEY ("customerId") REFERENCES "customer"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
		)
	}
}
