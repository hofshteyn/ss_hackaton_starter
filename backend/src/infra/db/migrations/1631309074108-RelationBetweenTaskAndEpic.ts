import { MigrationInterface, QueryRunner } from "typeorm"

export class RelationBetweenTaskAndEpic1631309074108 implements MigrationInterface {
	name = "RelationBetweenTaskAndEpic1631309074108"

	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`ALTER TABLE "public"."task" RENAME COLUMN "epic" TO "epicId"`)
		await queryRunner.query(
			`ALTER TABLE "public"."task" RENAME CONSTRAINT "UQ_929a4ba247d50034d52549f2b76" TO "UQ_6bb3a86bf4d828d74bffa275ced"`
		)
		await queryRunner.query(`ALTER TABLE "public"."task" DROP CONSTRAINT "UQ_6bb3a86bf4d828d74bffa275ced"`)
		await queryRunner.query(`ALTER TABLE "public"."task" DROP COLUMN "epicId"`)
		await queryRunner.query(`ALTER TABLE "public"."task" ADD "epicId" uuid`)
		await queryRunner.query(
			`ALTER TABLE "public"."task" ADD CONSTRAINT "FK_6bb3a86bf4d828d74bffa275ced" FOREIGN KEY ("epicId") REFERENCES "epic"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
		)
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`ALTER TABLE "public"."task" DROP CONSTRAINT "FK_6bb3a86bf4d828d74bffa275ced"`)
		await queryRunner.query(`ALTER TABLE "public"."task" DROP COLUMN "epicId"`)
		await queryRunner.query(`ALTER TABLE "public"."task" ADD "epicId" character varying NOT NULL`)
		await queryRunner.query(
			`ALTER TABLE "public"."task" ADD CONSTRAINT "UQ_6bb3a86bf4d828d74bffa275ced" UNIQUE ("epicId")`
		)
		await queryRunner.query(
			`ALTER TABLE "public"."task" RENAME CONSTRAINT "UQ_6bb3a86bf4d828d74bffa275ced" TO "UQ_929a4ba247d50034d52549f2b76"`
		)
		await queryRunner.query(`ALTER TABLE "public"."task" RENAME COLUMN "epicId" TO "epic"`)
	}
}
