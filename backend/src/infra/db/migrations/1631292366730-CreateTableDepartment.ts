import { MigrationInterface, QueryRunner } from "typeorm"

export class CreateTableDepartment1631292366730 implements MigrationInterface {
	name = "CreateTableDepartment1631292366730"

	public async up(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(
			`CREATE TABLE "department" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "name" character varying NOT NULL, CONSTRAINT "PK_9a2213262c1593bffb581e382f5" PRIMARY KEY ("id"))`
		)
		await queryRunner.query(`ALTER TABLE "public"."employee" ADD "departmentId" uuid`)
		await queryRunner.query(
			`ALTER TABLE "public"."employee" ADD CONSTRAINT "FK_9ad20e4029f9458b6eed0b0c454" FOREIGN KEY ("departmentId") REFERENCES "department"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
		)
	}

	public async down(queryRunner: QueryRunner): Promise<void> {
		await queryRunner.query(`ALTER TABLE "public"."employee" DROP CONSTRAINT "FK_9ad20e4029f9458b6eed0b0c454"`)
		await queryRunner.query(`ALTER TABLE "public"."employee" DROP COLUMN "departmentId"`)
		await queryRunner.query(`DROP TABLE "department"`)
	}
}
