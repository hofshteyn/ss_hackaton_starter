import { Database } from "../index"
import { CreateDepartments } from "./seeds/CreateDepartments"
import { Logger } from "../../logger"
import { CreateRegions } from "./seeds/CreateRegions"
import { CreateCustomers } from "./seeds/CreateCustomers"
import { CreateEmployees } from "./seeds/CreateEmployees"
import { CreateEpics } from "./seeds/CreateEpics"
import { CreateTasks } from "./seeds/CreateTasks"
import { CreateInterviews } from "./seeds/CreateInterviews"
import { CreateTaskTime } from "./seeds/CreateTaskTime"

async function seed(): Promise<void> {
	Logger.clean()
	Logger.log("Seed", "Started")

	await Database.connect()

	const seeds = [
		new CreateDepartments(),
		new CreateRegions(),
		new CreateCustomers(),
		new CreateEmployees(),
		new CreateEpics(),
		new CreateTasks(),
		new CreateTaskTime(),
		new CreateInterviews(),
	]
	for (const seed of seeds) {
		await seed.apply()
	}

	Logger.success("Seed", "Finished")
}
seed().then()
