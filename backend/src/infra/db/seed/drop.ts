import { createConnection } from "typeorm"
import { Database } from "../index"
import { Logger } from "../../logger"

export async function drop(): Promise<void> {
	Logger.clean()
	Database.connection = await createConnection()
	await Database.connection.dropDatabase()
}
drop().then(() => Logger.success("Seed", "End"))
