import { Seed } from "../core/Seed"
import citiesJSON from "../../../data/cities.json"
import { getRepository } from "typeorm"
import { City } from "../../../../domain/cities/entities/City"
import { Employee } from "../../../../domain/employees/entities/Employee"
import faker from "faker"
import { Random } from "../../../modules/random"
import { EmployeeSex } from "../../../../domain/employees/constants/EmployeeSex"
import { Department } from "../../../../domain/department/entities/Department"
import departmentsJSON from "../../../data/departments.json"

const data: [string, number][] = [
	["PM", 15],
	["Frontend", 174],
	["Web", 113],
	["SDET DevOps", 82],
	["Mobile", 77],
	["QA", 278],
	["Backend", 232],
	["Analytics", 49],
	["HR", 44],
	["IT Support", 8],
	["Accounting", 25],
	["Напр.бизнес-решений", 2],
	["СБП", 11],
	["Напр. Технической поддержки", 6],
	["Design", 20],
	["PR", 17],
	["Отдел продаж", 21],
	["Напр. внутренних проектов автоматизации", 8],
	["Фин.", 8],
	["Юр", 8],
]

export class CreateEmployees extends Seed {
	async apply(): Promise<void> {
		for (const c of citiesJSON) {
			const city = await getRepository(City).findOneOrFail({ name: c.city })

			for (let i = 0; i < c.amount; i++) {
				const randomElement = data[Math.floor(Math.random() * data.length)]
				const department = await getRepository(Department).findOne({ name: randomElement[0] })

				await getRepository(Employee)
					.create({
						name: faker.name.firstName() + " " + faker.name.lastName() + " " + faker.name.middleName(),
						login: faker.internet.userName(),
						sex: Random.fromEnum(EmployeeSex),
						age: Random.fromIntervalInt(19, 40),
						employment: faker.date.past(5),
						city,
						department,
					})
					.save()
			}
		}
	}
}
