import { Seed } from "../core/Seed"
import { getRepository } from "typeorm"
import { Epic } from "../../../../domain/epic/entities/Epic"
import { Random } from "../../../modules/random"
import { Interview } from "../../../../domain/interviews/entities/Interview"
import { InterviewStatus } from "../../../../domain/interviews/constants/InterviewStatus"
import { InterviewResult } from "../../../../domain/interviews/constants/InterviewResult"
import { Employee } from "../../../../domain/employees/entities/Employee"
import faker from "faker"

export class CreateInterviews extends Seed {
	async apply(): Promise<void> {
		const epics = await getRepository(Epic).find()
		for (const epic of epics) {
			const tasksAmount = Random.fromIntervalInt(1, 3)
			for (let i = 0; i < tasksAmount; i++) {
				const employee = await getRepository(Employee)
					.createQueryBuilder("e")
					.orderBy("RANDOM()")
					.limit(1)
					.getOne()

				await getRepository(Interview)
					.create({
						date: faker.date.past(1),
						status: Random.fromEnum(InterviewStatus),
						result: Random.fromEnum(InterviewResult),
						employee,
						epic,
					})
					.save()
			}
		}
	}
}
