import { Seed } from "../core/Seed"
import { getRepository } from "typeorm"
import { Customer } from "../../../../domain/customer/entities/Customer"
import { Epic } from "../../../../domain/epic/entities/Epic"
import faker from "faker"

export class CreateEpics extends Seed {
	async apply(): Promise<void> {
		const customers = await getRepository(Customer).find()
		for (const customer of customers) {
			await getRepository(Epic)
				.create({
					name: faker.name.title(),
					customer,
				})
				.save()
		}
	}
}
