import { Seed } from "../core/Seed"
import citiesJSON from "../../../data/cities.json"
import { getRepository } from "typeorm"
import { Region } from "../../../../domain/regions/entities/Region"
import { City } from "../../../../domain/cities/entities/City"

export class CreateRegions extends Seed {
	async apply(): Promise<void> {
		for (const city of citiesJSON) {
			if (city.region) {
				let region = await getRepository(Region).findOne({ name: city.region })
				if (!region) {
					region = await getRepository(Region).create({ name: city.region }).save()
				}
				await getRepository(City).create({ name: city.city, region }).save()
			}
		}
	}
}
