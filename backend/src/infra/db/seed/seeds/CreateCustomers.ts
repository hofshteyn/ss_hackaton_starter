import { Seed } from "../core/Seed"
import { getRepository } from "typeorm"
import { Customer } from "../../../../domain/customer/entities/Customer"
import faker from "faker"
import { Random } from "../../../modules/random"
import { CustomerType } from "../../../../domain/customer/constants/CustomerType"

export class CreateCustomers extends Seed {
	async apply(): Promise<void> {
		for (let i = 0; i < 50; i++) {
			await getRepository(Customer)
				.create({
					name: faker.company.companyName(),
					description: faker.random.words(20),
					type: Random.fromEnum(CustomerType),
				})
				.save()
		}
	}
}
