import { Seed } from "../core/Seed"
import { getRepository } from "typeorm"
import { Department } from "../../../../domain/department/entities/Department"

export class CreateDepartments extends Seed {
	async apply(): Promise<void> {
		const deps = [
			"PM",
			"Frontend",
			"Web",
			"SDET DevOps",
			"Mobile",
			"QA",
			"Backend",
			"Analytics",
			"HR",
			"IT Support",
			"Accounting",
			"Напр.бизнес-решений",
			"СБП",
			"Напр. Технической поддержки",
			"Design",
			"PR",
			"Отдел продаж",
			"Напр. внутренних проектов автоматизации",
			"Фин.",
			"Юр",
		]
		for (const d of deps) {
			await getRepository(Department)
				.create({
					name: d,
				})
				.save()
		}
	}
}
