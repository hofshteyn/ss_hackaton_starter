import { Seed } from "../core/Seed"
import { getRepository } from "typeorm"
import { Task } from "../../../../domain/tasks/entities/Task"
import { Random } from "../../../modules/random"
import { Employee } from "../../../../domain/employees/entities/Employee"
import { TaskEmployee } from "../../../../domain/tasks/entities/TaskEmployee"

export class CreateTaskTime extends Seed {
	async apply(): Promise<void> {
		const tasks = await getRepository(Task).find()
		for (const task of tasks) {
			const employeesAmount = Random.fromIntervalInt(1, 3)
			const employees: Employee[] = []
			while (employees.length < employeesAmount) {
				const employee = await getRepository(Employee)
					.createQueryBuilder("e")
					.orderBy("RANDOM()")
					.limit(1)
					.getOneOrFail()
				const exist = employees.find((e) => e.id === employee.id)
				if (!exist) {
					employees.push(employee)
				}
			}

			task.employees = [...employees]
			await task.save()

			for (const employee of employees) {
				await getRepository(TaskEmployee)
					.create({
						time: Random.fromIntervalInt(1, 5),
						employee,
						task,
					})
					.save()
			}
		}
	}
}
