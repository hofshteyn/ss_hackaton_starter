import { Seed } from "../core/Seed"
import { getRepository } from "typeorm"
import { Epic } from "../../../../domain/epic/entities/Epic"
import { Random } from "../../../modules/random"
import { Task } from "../../../../domain/tasks/entities/Task"
import faker from "faker"
import { TaskStatus } from "../../../../domain/tasks/constants/TaskStatus"
import { TaskType } from "../../../../domain/tasks/constants/TaskType"
import dayjs from "dayjs"

export class CreateTasks extends Seed {
	async apply(): Promise<void> {
		const epics = await getRepository(Epic).find()
		for (const epic of epics) {
			const tasksAmount = Random.fromIntervalInt(1, 5)
			for (let i = 0; i < tasksAmount; i++) {
				const startedDate = dayjs(faker.date.past(2))
				const duration = Random.fromIntervalInt(1, 5)
				const endDate = dayjs(startedDate).add(duration, "d")

				await getRepository(Task)
					.create({
						number: faker.random.alphaNumeric(8),
						status: Random.fromEnum(TaskStatus),
						type: Random.fromEnum(TaskType),
						startDate: startedDate,
						endDate: endDate,
						epic,
					})
					.save()
			}
		}
	}
}
