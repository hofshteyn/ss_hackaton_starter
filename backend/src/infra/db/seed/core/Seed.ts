export abstract class Seed {
	abstract apply(): Promise<void>
}
