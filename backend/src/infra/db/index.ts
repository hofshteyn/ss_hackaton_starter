import { Connection, createConnection } from "typeorm"
import { Logger } from "../logger"

export class Database {
	static connection: Connection

	static async connect(): Promise<void> {
		try {
			Database.connection = await createConnection()
			await Database.connection.runMigrations()
			if (process.env.NODE_ENV !== "test") {
				Logger.success("Database", "Connected")
			}
		} catch (error: any) {
			Logger.error("Database", error.message)
		}
	}
}
