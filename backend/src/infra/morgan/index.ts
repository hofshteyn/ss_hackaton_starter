import morgan from "morgan"
import { Request, Response } from "express"
import { format } from "date-fns"

morgan.token("customTime", (req: Request, res: Response) => {
	return format(Date.now(), "MM/dd/yyyy HH:mm:ss")
})
