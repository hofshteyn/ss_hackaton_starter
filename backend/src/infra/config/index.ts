export const Config = {
	PORT: loadEnv("PORT"),
	HOST: loadEnv("HOST"),
	NODE_ENV: loadEnv("NODE_ENV"),
}

function loadEnv(name: string) {
	const envVar = process.env[name]
	if (!envVar) {
		throw new Error(`${name} must be provided`)
	}
	return envVar
}
