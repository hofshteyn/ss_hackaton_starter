import { format } from "date-fns"
import chalk from "chalk"

export class Logger {
	private static timeFormat = "MM/dd/yyyy HH:mm:ss"

	static log(tag: string, message: string): void {
		const colorized = chalk.blue(`${format(Date.now(), this.timeFormat)} [${tag}]: ${message}`)
		console.log(colorized)
	}

	static success(tag: string, message: string): void {
		const colorized = chalk.green(`${format(Date.now(), this.timeFormat)} [${tag}]: ${message}`)
		console.log(colorized)
	}

	static error(tag: string, message: string): void {
		const colorized = chalk.red(`${format(Date.now(), this.timeFormat)} [${tag}]: ${message}`)
		console.log(colorized)
	}

	static clean(): void {
		console.clear()
	}
}
