import { HttpServer } from "./app/http/HttpServer"
import { Database } from "./infra/db"
import { Logger } from "./infra/logger"

export class Application {
	static async main(): Promise<void> {
		Logger.clean()
		Logger.log("Application", "Running")
		await Database.connect()
		await HttpServer.up()
	}
}
Application.main()
