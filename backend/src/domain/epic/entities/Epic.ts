import { BaseEntity, Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm"
import { Task } from "../../tasks/entities/Task"
import { Interview } from "../../interviews/entities/Interview"
import { Customer } from "../../customer/entities/Customer"

@Entity("epic")
export class Epic extends BaseEntity {
	@PrimaryGeneratedColumn("uuid")
	id!: string

	@Column("varchar", { nullable: false })
	name!: string

	@OneToMany(() => Task, (task) => task.epic)
	tasks!: Task[]

	@OneToMany(() => Interview, (interview) => interview.epic)
	interviews!: Interview[]

	@ManyToOne(() => Customer, (customer) => customer.epics)
	customer!: Customer
}
