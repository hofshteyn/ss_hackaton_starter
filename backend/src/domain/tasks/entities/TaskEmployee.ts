import { BaseEntity, Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm"
import { Task } from "./Task"
import { Employee } from "../../employees/entities/Employee"

@Entity("task_employee")
export class TaskEmployee extends BaseEntity {
	@PrimaryGeneratedColumn("uuid")
	id!: string

	@Column("float", { nullable: false })
	time!: number

	@ManyToOne(() => Task)
	task!: Task

	@ManyToOne(() => Employee)
	employee!: Employee
}
