import { BaseEntity, Column, Entity, ManyToMany, ManyToOne, PrimaryColumn } from "typeorm"
import { TaskStatus } from "../constants/TaskStatus"
import { TaskType } from "../constants/TaskType"
import { Customer } from "../../customer/entities/Customer"
import { Epic } from "../../epic/entities/Epic"
import { Employee } from "../../employees/entities/Employee"

@Entity("task")
export class Task extends BaseEntity {
	@PrimaryColumn("varchar")
	number!: string

	@Column("enum", { nullable: false, enum: TaskStatus, default: TaskStatus.Pending })
	status!: TaskStatus

	@Column("enum", { nullable: false, enum: TaskType })
	type!: TaskType

	@Column("timestamp", { nullable: false })
	startDate!: Date

	@Column("timestamp", { nullable: false })
	endDate!: Date

	@ManyToOne(() => Epic, (epic) => epic.tasks)
	epic!: Epic

	@ManyToMany(() => Employee, (employee) => employee.tasks)
	employees!: Employee[]
}
