export enum TaskStatus {
	Pending = 1,
	Processed,
	Done,
	Rejected,
	Incident,
}
