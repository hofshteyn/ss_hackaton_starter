import {
	BaseEntity,
	Column,
	Entity,
	JoinTable,
	ManyToMany,
	ManyToOne,
	OneToMany,
	PrimaryGeneratedColumn,
} from "typeorm"
import { City } from "../../cities/entities/City"
import { Department } from "../../department/entities/Department"
import { Interview } from "../../interviews/entities/Interview"
import { EmployeeSex } from "../constants/EmployeeSex"
import { Task } from "../../tasks/entities/Task"

@Entity("employee")
export class Employee extends BaseEntity {
	@PrimaryGeneratedColumn("uuid")
	id!: string

	@Column("varchar", { nullable: false })
	name!: string

	@Column("int", { nullable: false })
	age!: number

	@Column("enum", { nullable: false, enum: EmployeeSex })
	sex!: EmployeeSex

	@Column("varchar", { nullable: false })
	login!: string

	@Column("timestamp", { nullable: false })
	employment!: Date

	@Column("timestamp", { nullable: true })
	dismissal!: Date | undefined

	@ManyToOne(() => City, (city) => city.employees)
	city!: City

	@ManyToOne(() => Department, (department) => department.employees)
	department!: Department

	@OneToMany(() => Interview, (interview) => interview.employee)
	interviews!: Interview[]

	@ManyToMany(() => Task, (task) => task.employees)
	@JoinTable()
	tasks!: Task[]
}
