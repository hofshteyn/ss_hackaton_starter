export enum TimeInterval {
	Week = 1,
	Month,
	Quarter,
	Year,
}

export const intervalToSQL = (interval: TimeInterval) => {
	if (interval === TimeInterval.Week) {
		return "now() - interval '1 week'"
	} else if (interval === TimeInterval.Month) {
		return "now() - interval '1 months'"
	} else if (interval === TimeInterval.Quarter) {
		return "now() - interval '2 months'"
	} else if (interval === TimeInterval.Year) {
		return "now() - interval '1 year'"
	}
}
