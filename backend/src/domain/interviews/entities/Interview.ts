import { BaseEntity, Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm"
import { InterviewStatus } from "../constants/InterviewStatus"
import { InterviewResult } from "../constants/InterviewResult"
import { Customer } from "../../customer/entities/Customer"
import { Employee } from "../../employees/entities/Employee"
import { Epic } from "../../epic/entities/Epic"

@Entity("interview")
export class Interview extends BaseEntity {
	@PrimaryGeneratedColumn("uuid")
	id!: string

	@Column("enum", { nullable: false, enum: InterviewStatus })
	status!: InterviewStatus

	@Column("enum", { nullable: false, enum: InterviewResult })
	result!: InterviewResult

	@Column("timestamp", { nullable: false })
	date!: Date

	@ManyToOne(() => Employee, (employee) => employee.interviews)
	employee!: Employee

	@ManyToOne(() => Epic, (epic) => epic.interviews)
	epic!: Epic
}
