export enum InterviewStatus {
	SearchingCandidate = 1,
	CandidateFound,
	InterviewEnded,
	InterviewRejected,
	WaitingInterview,
}
