export enum InterviewResult {
	InterviewFailed = 1,
	InterviewPassed,
	CandidateWaiting,
	CandidateProcessed,
}
