import { BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm"
import { Employee } from "../../employees/entities/Employee"

@Entity("department")
export class Department extends BaseEntity {
	@PrimaryGeneratedColumn("uuid")
	id!: string

	@Column("varchar")
	name!: string

	@OneToMany(() => Employee, (employee) => employee.department)
	employees!: Employee[]
}
