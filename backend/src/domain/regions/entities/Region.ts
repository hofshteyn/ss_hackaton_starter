import { BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm"
import { City } from "../../cities/entities/City"

@Entity("region")
export class Region extends BaseEntity {
	@PrimaryGeneratedColumn("uuid")
	id!: string

	@Column("varchar", { nullable: false })
	name!: string

	@OneToMany(() => City, (city) => city.region)
	cities!: City[]
}
