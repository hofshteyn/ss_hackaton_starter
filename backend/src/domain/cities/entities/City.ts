import { BaseEntity, Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm"
import { Region } from "../../regions/entities/Region"
import { Employee } from "../../employees/entities/Employee"

@Entity("city")
export class City extends BaseEntity {
	@PrimaryGeneratedColumn("uuid")
	id!: string

	@Column("varchar", { nullable: false })
	name!: string

	@ManyToOne(() => Region, (region) => region.cities)
	region!: Region

	@OneToMany(() => Employee, (employee) => employee.city)
	employees!: Employee[]
}
