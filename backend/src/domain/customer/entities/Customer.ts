import { BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm"
import { CustomerType } from "../constants/CustomerType"
import { Task } from "../../tasks/entities/Task"
import { InterviewResult } from "../../interviews/constants/InterviewResult"
import { Interview } from "../../interviews/entities/Interview"
import { Epic } from "../../epic/entities/Epic"

@Entity("customer")
export class Customer extends BaseEntity {
	@PrimaryGeneratedColumn("uuid")
	id!: string

	@Column("varchar", { nullable: false })
	name!: string

	@Column("enum", { nullable: false, enum: CustomerType })
	type!: CustomerType

	@Column("text")
	description!: string

	@OneToMany(() => Epic, (epic) => epic.customer)
	epics!: Epic[]
}
